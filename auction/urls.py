from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r"^getManufs/", getManufs),
    url(r"^getModels/(?P<manuf_id>\d+)/$", getModels),
    url(r"^getTypes/(?P<model_id>\d+)/$", getTypes),
    url(r"^getSections/$", getSections),
]
