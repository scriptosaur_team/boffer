from django.db import models
from django.utils.translation import ugettext_lazy as _
from user.models import BofferUser


class Auction(models.Model):
    user = models.ForeignKey(BofferUser, verbose_name=_('user'))
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    name = models.CharField(_('name'), max_length=512)
    item = models.CharField(_('part or material type'), max_length=512)
    manufacturer = models.CharField(_('manufacturer'), max_length=512)

    def __str__(self):
        return "{} / {}".format(self.name, self.item)

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('auction')
        verbose_name_plural = _('auctions')
