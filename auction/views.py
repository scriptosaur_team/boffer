# -*- coding: UTF-8 -*-
from django.shortcuts import render
from django.http.response import HttpResponse
from django.http import JsonResponse
import MySQLdb
# Create your views here.
def getManufs(request):
  tecdoc = MySQLdb.connect(user='root', db='tecdoc2015q4', passwd='EmHnTJ', host='localhost',use_unicode = 1, charset = 'utf8')
  tecdoc.set_character_set('utf8')
  cursor = tecdoc.cursor()
  cursor.execute('SET NAMES utf8')
  cursor.execute('SET CHARACTER SET utf8')
  cursor.execute('SET character_set_connection=utf8')

  cursor.execute('SELECT MFA_BRAND, MFA_MFC_CODE, MFA_ID FROM manufacturers ORDER BY MFA_BRAND')
  # names = [row[0] for row in cursor.fetchall()]
  strManuf = ""
  for manuf in cursor:
    strManuf += "<li data-manuf_id='"+str(manuf[2])+"'><span>"+manuf[0]+"</span></li>"
  return HttpResponse(strManuf)

def getModels(request, manuf_id):
  tecdoc = MySQLdb.connect(user='root', db='tecdoc2015q4', passwd='WR2YPZ', host='localhost',use_unicode = 1, charset = 'utf8')
  tecdoc.set_character_set('utf8')
  cursor = tecdoc.cursor()
  cursor = tecdoc.cursor()
  cursor.execute('SET NAMES utf8')
  cursor.execute('SET CHARACTER SET utf8')
  cursor.execute('SET character_set_connection=utf8')
  sql = 'SELECT MOD_ID, TEX_TEXT AS MOD_CDS_TEXT, MOD_PCON_START, MOD_PCON_END FROM MODELS INNER JOIN COUNTRY_DESIGNATIONS ON CDS_ID = MOD_CDS_ID INNER JOIN DES_TEXTS ON TEX_ID = CDS_TEX_ID WHERE MOD_MFA_ID = '+str(manuf_id)+' AND CDS_LNG_ID = 16 ORDER BY MOD_CDS_TEXT'
  cursor.execute(sql)
  strModels = ""
  for model in cursor:
    yearStart = str(model[2])
    if len(yearStart)>4:
        yearStart = yearStart[:-2]
    strModels += "<li data-model_id='"+str(model[0])+"'><span>"+model[1]+" модель "+yearStart+" г.в.</span></li>"
  return HttpResponse(strModels)

def getTypes(request, model_id):
  sql="SELECT TYP_ID, DES_TEXTS.TEX_TEXT AS TYP_CDS_TEXT, TYP_PCON_START, TYP_PCON_END, TYP_CCM, TYP_KW_FROM, TYP_KW_UPTO, TYP_HP_FROM, TYP_HP_UPTO, TYP_CYLINDERS, ENGINES.ENG_CODE, DES_TEXTS2.TEX_TEXT AS TYP_ENGINE_DES_TEXT, DES_TEXTS3.TEX_TEXT AS TYP_FUEL_DES_TEXT, IFNULL(DES_TEXTS4.TEX_TEXT, DES_TEXTS5.TEX_TEXT) AS TYP_BODY_DES_TEXT, DES_TEXTS6.TEX_TEXT AS TYP_AXLE_DES_TEXT, TYP_MAX_WEIGHT FROM TYPES					INNER JOIN COUNTRY_DESIGNATIONS ON COUNTRY_DESIGNATIONS.CDS_ID = TYP_CDS_ID AND COUNTRY_DESIGNATIONS.CDS_LNG_ID = 16 INNER JOIN DES_TEXTS ON DES_TEXTS.TEX_ID = COUNTRY_DESIGNATIONS.CDS_TEX_ID					LEFT JOIN DESIGNATIONS ON DESIGNATIONS.DES_ID = TYP_KV_ENGINE_DES_ID AND DESIGNATIONS.DES_LNG_ID = 16 LEFT JOIN DES_TEXTS AS DES_TEXTS2 ON DES_TEXTS2.TEX_ID = DESIGNATIONS.DES_TEX_ID	LEFT JOIN DESIGNATIONS AS DESIGNATIONS2 ON DESIGNATIONS2.DES_ID = TYP_KV_FUEL_DES_ID AND DESIGNATIONS2.DES_LNG_ID = 16 LEFT JOIN DES_TEXTS AS DES_TEXTS3 ON DES_TEXTS3.TEX_ID = DESIGNATIONS2.DES_TEX_ID LEFT JOIN LINK_TYP_ENG ON LTE_TYP_ID = TYP_ID	LEFT JOIN ENGINES ON ENG_ID = LTE_ENG_ID LEFT JOIN DESIGNATIONS AS DESIGNATIONS3 ON DESIGNATIONS3.DES_ID = TYP_KV_BODY_DES_ID AND DESIGNATIONS3.DES_LNG_ID = 16 LEFT JOIN DES_TEXTS AS DES_TEXTS4 ON DES_TEXTS4.TEX_ID = DESIGNATIONS3.DES_TEX_ID LEFT JOIN DESIGNATIONS AS DESIGNATIONS4 ON DESIGNATIONS4.DES_ID = TYP_KV_MODEL_DES_ID AND DESIGNATIONS4.DES_LNG_ID = 16 LEFT JOIN DES_TEXTS AS DES_TEXTS5 ON DES_TEXTS5.TEX_ID = DESIGNATIONS4.DES_TEX_ID LEFT JOIN DESIGNATIONS AS DESIGNATIONS5 ON DESIGNATIONS5.DES_ID = TYP_KV_AXLE_DES_ID AND DESIGNATIONS5.DES_LNG_ID = 16 LEFT JOIN DES_TEXTS AS DES_TEXTS6 ON DES_TEXTS6.TEX_ID = DESIGNATIONS5.DES_TEX_ID WHERE TYP_MOD_ID = "+str(model_id)+" ORDER BY TYP_SORT, TYP_CDS_TEXT, TYP_PCON_START, TYP_CCM"
  tecdoc = MySQLdb.connect(user='root', db='tecdoc2015q4', passwd='EmHnTJ', host='localhost',use_unicode = 1, charset = 'utf8')
  tecdoc.set_character_set('utf8')
  cursor = tecdoc.cursor()
  cursor = tecdoc.cursor()
  cursor.execute('SET NAMES utf8')
  cursor.execute('SET CHARACTER SET utf8')
  cursor.execute('SET character_set_connection=utf8')
  cursor.execute(sql)
  # test = cursor.fetchall()
  strTypes = ""
  for modType in cursor:
      strEng = ""
      if modType[11]:
          strEng = modType[11]
      strEngCode = ""
      if modType[10]:
          strEngCode = modType[10]
      strTypes += "<li data-type_id='"+str(modType[0])+"'><span>"+str(modType[1])+" "+strEngCode+" "+strEng+"</span></li>"
  return HttpResponse(strTypes)

def getSections(request):
    #Выберем родительские
    type_id = request.GET.get('type_id')
    #type_id = 10001

    tecdoc = MySQLdb.connect(user='root', db='tecdoc2015q4', passwd='EmHnTJ', host='localhost',use_unicode = 1, charset = 'utf8')
    tecdoc.set_character_set('utf8')
    cursor = tecdoc.cursor()
    cursor = tecdoc.cursor()
    cursor.execute('SET NAMES utf8')
    cursor.execute('SET CHARACTER SET utf8')
    cursor.execute('SET character_set_connection=utf8')
    sql='SELECT	STR_ID, TEX_TEXT AS STR_DES_TEXT, STR_ID_PARENT, STR_LEVEL, STR_SORT, STR_NODE_NR,	IF(EXISTS(SELECT * FROM SEARCH_TREE AS SEARCH_TREE2 WHERE SEARCH_TREE2.STR_ID_PARENT <=> SEARCH_TREE.STR_ID LIMIT 1), 1, 0) AS DESCENDANTS FROM SEARCH_TREE	INNER JOIN DESIGNATIONS ON DES_ID = STR_DES_ID INNER JOIN DES_TEXTS ON TEX_ID = DES_TEX_ID	WHERE STR_ID_PARENT <=> '+str(request.GET.get('section_id'))+' AND DES_LNG_ID = 16 AND EXISTS (SELECT * FROM LINK_GA_STR INNER JOIN LINK_LA_TYP ON LAT_TYP_ID ='+str(type_id)+' AND LAT_GA_ID = LGS_GA_ID INNER JOIN LINK_ART ON LA_ID = LAT_LA_ID	WHERE LGS_STR_ID = STR_ID LIMIT 1) ORDER BY STR_DES_TEXT'
    cursor.execute(sql)
    #test = cursor.fetchall()
    #root_id = []
    if cursor.rowcount == 0:
        sql = 'SELECT LA_ART_ID, ART_ARTICLE_NR	FROM LINK_GA_STR INNER JOIN LINK_LA_TYP ON LAT_TYP_ID = '+str(request.GET.get('type_id'))+' AND LAT_GA_ID = LGS_GA_ID INNER JOIN LINK_ART ON LA_ID = LAT_LA_ID INNER JOIN ARTICLES ON ART_ID = LA_ART_ID WHERE LGS_STR_ID = '+request.GET.get('section_id')+' ORDER BY LA_ART_ID LIMIT 1000';
        cursor.execute(sql)
        #strSections = cursor.fetchall()
        sql = "SELECT ART_ID, ART_ARTICLE_NR, SUP_BRAND, SUP_ID, DES_TEXTS.TEX_TEXT AS ART_COMPLETE_DES_TEXT FROM ARTICLES INNER JOIN DESIGNATIONS ON DESIGNATIONS.DES_ID = ART_COMPLETE_DES_ID	AND DESIGNATIONS.DES_LNG_ID = 16 INNER JOIN DES_TEXTS ON DES_TEXTS.TEX_ID = DESIGNATIONS.DES_TEX_ID INNER JOIN SUPPLIERS ON SUP_ID = ART_SUP_ID WHERE ART_ID = "
        for part in cursor:
            sql = sql+str(part[0])+" OR ART_ID = "
        # sql = sql[:-13]
        sql = sql[:-15]
        cursor.execute(sql)
        strResponse = ""
        for part in cursor:
            strResponse +="<li class='demo'><div class='text-wrap'><span>"+part[2]+"</span><p>"+part[4]+"</p></div><div class='choos-button choos-disable'>Выбрать</div></li>"
        return JsonResponse({'type':'parts','str':strResponse})
    else:
        strSections = "<ul class='nasting'>";
        for section in cursor:
            if section[0]==13771:
                continue
            strSections += "<li data-section_id='"+str(section[0])+"' class='list-element active'><span>"+str(section[1])+"</span></li>"
        strSections += "</ul>"
    return JsonResponse({'type':'sections','str':strSections})
