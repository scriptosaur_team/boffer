from django.contrib import admin
from .models import *


@admin.register(Auction)
class AuctionAdmin(admin.ModelAdmin):

    list_display = ['created_at', 'user', 'name', 'item', 'manufacturer']
