from rest_framework import serializers
from tecdoc.models import Manufacturer, Model, Type, Section, Part


class ManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manufacturer
        fields = [
            'id',
            'mfa_brand',
        ]


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = [
            'id',
            'year_start',
            'text',
        ]


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = [
            'id',
            'description',
        ]


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = [
            'id',
            'text',
        ]


class PartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Part
        fields = [
            'id',
            'brand',
            'text',
        ]
