from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from .views import *


router = DefaultRouter()
router.register(r'manufacturers', ManufacturerViewSet)
router.register(r'models', ModelViewSet)
router.register(r'types', TypeViewSet)


urlpatterns = [
                  url(r"^section/$", SectionView.as_view()),
              ] + router.urls
