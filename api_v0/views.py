from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from tecdoc.models import Manufacturer, Model, Type, Section, Part
from .serializers import *


class ManufacturerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Manufacturer.objects.all()
    lookup_field = 'id'
    serializer_class = ManufacturerSerializer


class ModelViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Model.objects.data()
    serializer_class = CarModelSerializer

    def get_queryset(self):
        try:
            manufacturer_id = int(self.request.query_params.get('manufacturer_id', None))
            return Model.objects.by_manufacturer(manufacturer_id)
        except TypeError:
            pass
        return self.queryset


class TypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Type.objects.data()
    serializer_class = TypeSerializer

    def get_queryset(self):
        try:
            model_id = int(self.request.query_params.get('model_id', None))
            return Type.objects.by_model(model_id)
        except TypeError:
            pass
        return self.queryset


class SectionView(APIView):

    def get(self, request, format=None):
        section_id = self.request.query_params.get('section_id', None)
        section_qs = Section.objects.data()
        part_qs = None

        try:
            type_id = self.request.query_params.get('type_id', None)
            section_qs = Section.objects.select(type_id, section_id)
            part_qs = Part.objects.select(type_id, section_id)
        except TypeError:
            pass
        result = {
            'sections': [SectionSerializer(s).data for s in section_qs],
            'parts': [PartSerializer(p).data for p in part_qs] if part_qs else None
        }
        return Response(result)
