//Переменные для харнения параметров создаваемого аукциона
var car_id = 0;
var model_id = 0;
var type_id = 0;
var arParts_id = [];


$(window).ready(function(){
    $(window).scroll(function(){
        var position = $(window).scrollTop();
        if((position>2300)&&(position<2800)) {
            $(".diagramma").each(function(){
                $(this).easyPieChart({
                    trackColor: 'rgba(0, 0, 0, 0)',
                    size: 159,
                    lineWidth: 7,
                    barColor: '#56C457',
                    scaleColor: "rgba(0, 0, 0, 0)",
                    onStep: function(from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    }
                });
                var chart = window.chart = $(this).data('easyPieChart');
                var obg = $(this).find(".top").html();
                chart.update(obg);
            });
        }
    });
    $('.slider').bxSlider({
    	pager: false,
    	control: true
    });
    var detailCounter = $(".detail-counter").html();
    var position;
    $(".create-auction").click(function(){
    	position = $("body").scrollTop();
    	position +=100;
    	$(".new-auction").css("top", position+"px");
    	var size = $("body").css("height");
    	$(".overlay").css("height", size);
    	$(".overlay").fadeIn();
    	$(".pop-up").fadeIn();
    	$("#buyer-window").fadeIn();
    	$(".model-span").html($(".model").val());
    	return false;
    });
    $(".model-span").html($(".model").val());
    var count = 0;
    var windowEl = $("#buyer-auto-detail .pop-up-window > li").each(function(){});
    var windowElWheel = $("#buyer-wheel .pop-up-window > li").each(function(){});
    var windowElPr = $("#provider-auto-detail .pop-up-window > li").each(function(){});
    var windowElWheelPr = $("#provider-wheel .pop-up-window > li").each(function(){});
    var masObject = $("#buyer-auto-detail .column").each(function(){});
    var masObjectWheel = $(".new-auction-wheel .column").each(function(){});
    var masObjectPr = $("#provider-auto-detail .column").each(function(){});
    var masObjectWheelPr = $("#provider-wheel .column").each(function(){});
    var masObjectSpan = [];
    $("#buyer-auto-detail .column span").each(function(index){
        masObjectSpan[index] = $(this).html();
    });
    var masObjectWheelSpan = [];
    $("#buyer-wheel .column span").each(function(index){
        masObjectWheelSpan[index] = $(this).html();
    });
     var closes = function(){
            wheelActive = 0;
            partsActive = 0;
            $(".overlay").fadeOut();
            $(".pop-up").fadeOut();
            $(".new-auction").fadeOut();
            $(".new-auction-auto-details").fadeOut();
            $(".new-auction-wheel").fadeOut();
            if ($(windowEl[0]).hasClass("pop-up-disable")||$(windowElWheel[0]).hasClass("pop-up-disable")) {
                $(windowEl[0]).removeClass("pop-up-disable").addClass("pop-up-enable");
                $(windowElWheel[0]).removeClass("pop-up-disable").addClass("pop-up-enable");
                var i = 1;
                $(masObject[0]).children().children(".hover").css("display", "none");
                $(masObjectWheel[0]).children().children(".hover").css("display", "none");
                while (windowEl[i]) {
                    if ($(windowEl[i]).hasClass("pop-up-enable")) {
                        $(windowEl[i]).removeClass("pop-up-enable").addClass("pop-up-disable");
                    }
                    if ($(windowElWheel[i]).hasClass("pop-up-enable")){
                        $(windowElWheel[i]).removeClass("pop-up-enable").addClass("pop-up-disable");
                    }
                    $(masObject[i]).css("opacity", "0.7");
                    $(masObjectWheel[i]).css("opacity", "0.7");

                    $(masObject[i]).children().children(".hover").css("display", "none");
                    $(masObjectWheel[i]).children().children(".hover").css("display", "none");
                    i++;
                }
            }
            if ($(windowElPr[0]).hasClass("pop-up-disable")||$(windowElWheelPr[0]).hasClass("pop-up-disable")) {
                $(windowElPr[0]).removeClass("pop-up-disable").addClass("pop-up-enable");
                $(windowElWheelPr[0]).removeClass("pop-up-disable").addClass("pop-up-enable");
                var i = 1;
                $(masObject[0]).children().children(".hover").css("display", "none");
                $(masObjectWheel[0]).children().children(".hover").css("display", "none");
                $(masObjectPr[0]).children().children(".hover").css("display", "none");
                $(masObjectWheelPr[0]).children().children(".hover").css("display", "none");
                while (windowElPr[i]) {
                    if ($(windowElPr[i]).hasClass("pop-up-enable")) {
                        $(windowElPr[i]).removeClass("pop-up-enable").addClass("pop-up-disable");
                    }
                    if ($(windowElWheelPr[i]).hasClass("pop-up-enable")){
                        $(windowElWheelPr[i]).removeClass("pop-up-enable").addClass("pop-up-disable");
                    }
                    $(masObjectPr[i]).css("opacity", "0.7");
                    $(masObjectWheelPr[i]).css("opacity", "0.7");

                    $(masObjectPr[i]).children().children(".hover").css("display", "none");
                    $(masObjectWheelPr[i]).children().children(".hover").css("display", "none");
                    i++;
                }
            }
            $("#buyer-auto-details .column span").each(function(index){
                $(this).html(masObjectSpan[index]);
            });
            $("#buyer-wheel .column span").each(function(index){
                $(this).html(masObjectWheelSpan[index]);
            });
            $("#provider-auto-detail .column span").each(function(index){
                $(this).html(masObjectSpan[index]);
            });
            $("#provider-wheel .column span").each(function(index){
                $(this).html(masObjectWheelSpan[index]);
            });
            $("input[type='text']").attr("value","");
            count = 0;
            $(".checked").each(function(){
                if($(this).hasClass("check-enable")) {
                    $(this).addClass("check-disable").removeClass("check-enable");
                }
            });
            $(".manuf-list").addClass("js-disable");
            $(".manuf-wrap").each(function(){
                if(!$(this).children(".country").hasClass("js-disable")) {
                   $(this).children(".country").addClass("js-disable");
                }

                $(this).children(".country-img").attr("src", "");
            });
            $(".selected-details").children("li").each(function(){
                if($(this).children(".choos-button").hasClass("choos-enable")) {
                    $(this).children(".choos-button").addClass("choos-disable").removeClass("choos-enable").html("Выбрать");
                }
                $(".select-details").append($(this));
            });
            $(".detail-counter").addClass("js-disable").html(0);
            $(".filter").each(function(){
                $(this).val("");
            });
        }
    $(".overlay").click(function(){
        closes();
    });
    $(".close").click(function(){
        closes();
    });
    var partsActive = 0;
    var wheelActive = 0;

    $("#auto-details-buyer").click(function(){
      partsActive = 1;

    	//$.get('/auction/getManufs/',function(data){
         //   $('#cars').html(data);
        //});
        $.get(
            '/api/v0/manufacturers/',
            function(data){
                $("#cars").empty();
                for(var i = 0; i < data.length; i++) {
                    $("#cars").append("<li data-manuf_id='"+data[i]['id']+"'><span>"+data[i]['mfa_brand']+"</span></li>")
                }
            }
        );
        $(".new-auction").fadeOut();
    	$(".new-auction-auto-details").css("top", position+"px");
    	$("#buyer-auto-detail").fadeIn();

    	return false;
    });
    var tabsAnimateFunc = function(selector){
        if ($(selector).hasClass("win-link")) {
            $(".win-link .pseudo-link").css("left", "175px");

            $(".win-link .pseudo-link").animate({left: 0}, 500);
        }
        if ($(selector).hasClass("lable-link")){
            $(".lable-link .pseudo-link").css("left", "-175px");
            $(".lable-link .pseudo-link").animate({left: 0}, 500);
        }
    }
    var tabsFunc = function(selector, context){
        context.parents(".pop-up-window li").find(".tabs").each(function(){
            if ($(this).hasClass("enable-tabs")){
                $(this).removeClass("enable-tabs").addClass("disable-tabs");
            }
        });
        $(selector).addClass("enable-tabs").removeClass("disable-tabs");
    };
    $(".alt-link").click(function(){
        if($(this).hasClass("disable-link")) {
            tabsAnimateFunc(this);
            $(this).parents(".pop-up-window li").find(".alt-link").each(function(){
                if($(this).hasClass("enable-link")){
                    $(this).removeClass("enable-link").addClass("disable-link");
                }
            });
            $(this).removeClass("disable-link").addClass("enable-link");
        }
        var name = "." + $(this).data("name");
        tabsFunc(name, $(this));
        return false;
    });
    // выбор детали и производителя
    $(document).on('click',".choos-button",function(){
        $(this).parents(".select-details-wrapper li").children().css("opacity", 0);
        var thisElem = this;
        setTimeout(function(){
                if ($(thisElem).hasClass("choos-disable")) {
                    detailCounter++;
                    $(".selected-details").append($(thisElem).parents(".select-details-wrapper li").children().css("opacity", 1).parent().get());
                    $(thisElem).removeClass("choos-disable").addClass("choos-enable").html("выбрано").addClass("in");
                    $(thisElem).attr("value",$(thisElem).parents("li").find(".text-wrap > span").html());
                }
                else {
                    $(thisElem).removeClass("choos-enable").addClass("choos-disable").html("выбрать").removeClass("in");
                    $(".select-details").append($(thisElem).parents(".select-details-wrapper li").children().css("opacity", 1).parent().get());
                    detailCounter--;
                }
                if($(".detail-counter").hasClass("js-disable")){
                    $(".detail-counter").removeClass("js-disable");
                }
                $(".detail-counter").html(detailCounter);
            }, 500);
    });
    $(".select-details-wrapper").niceScroll({
    	cursorcolor: "#B8B8B8",
    	cursoropacitymin: "0.5",
    	cursorwidth: "10px",
    });
    //-----переключение формы
    var obj = masObject[0];
    var objWheel = masObjectWheel[0];
    var objPr = masObjectPr[0];
    var objWheelPr = masObjectWheelPr[0];
    $(obj).css("opacity", "1");
    $(objWheel).css("opacity", "1");
    $(obj).children().children(".norm").css("opacity", "1");
    $(objWheel).children().children(".norm").css("opacity", "1");
    $(objPr).css("opacity", "1");
    $(objWheelPr).css("opacity", "1");
    $(objPr).children().children(".norm").css("opacity", "1");
    $(objWheelPr).children().children(".norm").css("opacity", "1");


    // кнопка далее
    $(".next-button").click(function(){
        switch (count) {
          case 0:// завершили Выбор атво
            if(partsActive == 1){
              console.log('car_id = '+car_id);
              console.log('type_id = '+type_id);
              console.log('model_id = '+model_id);
              //$.get('/auction/getSections',{'type_id':type_id,section_id:10001},function(data){
              //  $('#root_menu').html(data.str);
              //},"json");
                $.getJSON(
                    "/api/v0/section/?type_id=" + type_id,
                    function(data){
                        var $list = $("<ul>").addClass("nasting"),
                            sections = data['sections'];
                        for(var i=0; i<sections.length; i++){
                            var s = sections[i];
                            $list.append("<li data-section_id='" + s['id'] + "' class='list-element active'><span>" + s['text'] + "</span></li>");
                        }
                        $('#root_menu').html($list);
                    }
                );

            }else{
              console.log('Это первый шаг по колесам');
            }
            break;
          case 1://завершили выбор запчастей
            if(partsActive == 1){
              console.log('Это второй шаг по запчастям');
            }else{
              console.log('Это второй шаг по колесам');
            }
            break;
          case 2:
              console.log('3');//Завершли выбор производителя шлем ajax на создание аукциона(ов)
              break;
          default:
          break;

        }
        $(masObject[count]).children("span").html($(this).parent().find(".in").attr("value"))
        $(masObjectWheel[count]).children("span").html($(this).parent().find(".in").attr("value"));
        $(masObjectPr[count]).children("span").html($(this).parent().find(".in").attr("value"))
        $(masObjectWheelPr[count]).children("span").html($(this).parent().find(".in").attr("value"));
    	$(this).parent().removeClass("pop-up-enable").addClass("pop-up-disable");
    	$(this).parent().next().removeClass("pop-up-disable").addClass("pop-up-enable");
    	var j = 0;
    	while(masObject[j]) {
    		$(masObject[j]).css("opacity", "0.7");
    		$(masObjectWheel[j]).css("opacity", "0.7");
    		$(masObject[j]).children().children(".hover").css("display", "none");
    		$(masObjectWheel[j]).children().children(".hover").css("display", "none");
    		j++;
    	}
        j = 0;
        while(masObjectPr[j]) {
            $(masObjectPr[j]).css("opacity", "0.7");
            $(masObjectWheelPr[j]).css("opacity", "0.7");
            $(masObjectPr[j]).children().children(".hover").css("display", "none");
            $(masObjectWheelPr[j]).children().children(".hover").css("display", "none");
            j++;
        }
    	count++;
		for (var i=0; i<=count; i++) {
			var obg = masObject[i];
			var obgWheel = masObjectWheel[i];
			$(obg).css("opacity", "1");
			$(obgWheel).css("opacity", "1");
			$(masObject[i-1]).children().children(".hover").css("display", "block");
			$(masObjectWheel[i-1]).children().children(".hover").css("display", "block");
		}
        for (var i=0; i<=count; i++) {
            var obgPr = masObjectPr[i];
            var obgWheelPr = masObjectWheelPr[i];
            $(obgPr).css("opacity", "1");
            $(obgWheelPr).css("opacity", "1");
            $(masObjectPr[i-1]).children().children(".hover").css("display", "block");
            $(masObjectWheelPr[i-1]).children().children(".hover").css("display", "block");
        }
     });
    $(".cansel").click(function(){
  		$(this).parent().removeClass("pop-up-enable").addClass("pop-up-disable");
  		$(this).parent().prev().removeClass("pop-up-disable").addClass("pop-up-enable");
  		count--;
  		var j = 0;
  		while(masObject[j]) {
  			$(masObject[j]).css("opacity", "0.7");
  			$(masObjectWheel[j]).css("opacity", "0.7");
  			$(masObject[j]).children().children(".hover").css("display", "none");
  			$(masObjectWheel[j]).children().children(".hover").css("display", "none");
  			j++;
  		}
        j = 0;
  		for (var i=0; i<=count; i++) {
  			var obg = masObject[i];
  			var obgWheel = masObjectWheel[i];
  			$(obg).css("opacity", "1");
  			$(obgWheel).css("opacity", "1");
  		}
        while(masObjectPr[j]) {
            $(masObjectPr[j]).css("opacity", "0.7");
            $(masObjectWheelPr[j]).css("opacity", "0.7");
            $(masObjectPr[j]).children().children(".hover").css("display", "none");
            $(masObjectWheelPr[j]).children().children(".hover").css("display", "none");
            j++;
        }
        for (var i=0; i<=count; i++) {
            var obgPr = masObjectPr[i];
            var obgWheelPr = masObjectWheelPr[i];
            $(obgPr).css("opacity", "1");
            $(obgWheelPr).css("opacity", "1");
        }
  		return false;
    });
    $(".registration-pop-up").click(function(){
    	$(".pop-up-element").fadeOut();
    	$(".registration-message").fadeIn();
    	$(".registration-message").css("top", position);
    	return false;
    });
    $("#wheel-buyer").click(function(){
      wheelActive = 1;
    	$(".new-auction").fadeOut();
    	$(".new-auction-wheel").css("top", position+"px");
    	$("#buyer-wheel").fadeIn();
    	return false;
    });
    $(".bus-link").click(function(){
    	if ($(".bus").css("display")=="none") {
    		$(".drives").fadeOut();
    		setTimeout(function(){
    			$(".bus").fadeIn();
    		}, 400);
    		return false;
    	}
    	else {
    		return false;
    	}
    });
    $(".drives-link").click(function(){
    	if ($(".drives").css("display")=="none") {
    		$(".bus").fadeOut();
    		setTimeout(function(){
    			$(".drives").fadeIn();
    		}, 400);
    		return false;
    	}
    	else {
    		return false;
    	}
    });
    $(".select-container input").focus(function(){
    	$(this).parent().children(".select-list").fadeIn();
    });
    //Клик по марке или модели
	$(document).on('click',".select-list li",function(){
        this_ul_id = $(this).parent().attr('id');
        if(this_ul_id == "cars"){
            car_id = $(this).attr('data-manuf_id');
            //$.get('/auction/getModels/'+car_id+'/',function(data){
            //  $('#models').html(data);
            //  $('#modelsInput').removeAttr('disabled');
            //});
            $.get(
                '/api/v0/models/?manufacturer_id=' + car_id,
                function(data){
                    $('#models').empty();
                    for(var i=0; i<data.length;i++){
                        var m = data[i];
                        $("#models").append($("<li data-model_id='"+ m.id +"'><span>"+ m['text'] +" модель " + m['year_start'] + " г.в.</span></li>"));
                    }
                    $('#modelsInput').removeAttr('disabled');
                }
            );
        }
        if(this_ul_id == "models"){
          model_id = $(this).attr('data-model_id');
          //$.get('/auction/getTypes/'+model_id+'/',function(data){
          //  $('#types').html(data);
          //  $('#typesInput').removeAttr('disabled');
          //});
            $.get(
                '/api/v0/types/?model_id=' + model_id,
                function(data){
                    $('#types').empty();
                    for(var i=0; i<data.length; i++){
                        var t = data[i];
                        $('#types').append($("<li data-type_id='" + t.id + "'><span>" + t['description'] + "</span></li>"));
                    }
                    $('#typesInput').removeAttr('disabled');
                }
            );
        }
        if(this_ul_id == "types")
          type_id = $(this).attr('data-type_id');

		$(this).parent().parent().children("input").val($(this).children("span").html());
		$(this).parent().fadeOut();
	});
	$(document).click(function(){
		if(!$(event.target).parent().hasClass("select-container")){
			$(".select-list").fadeOut();
		}
	});
	$(".select-list").niceScroll({
		cursorcolor: "#B8B8B8",
		cursoropacitymin: "0.5",
		cursorwidth: "10px",
		railoffset: {top: -12, left: 0}
	});
    $(".header-menu li").click(function(){
        if(!$(this).hasClass("lk-active")){
            $(".header-menu li").each(function(){
                if($(this).hasClass("lk-active")) {
                    $(this).removeClass("lk-active");
                }
            });
            $(this).addClass("lk-active");
        }
    });
    $(".qur").click(function(){
        if ($(".tab-qur").css("display")=="block"){
            return false;
        }
        else {
            $(".tab-qur").fadeIn();
            $(".qur").css("background", "#ffffff");
            $(".tab-fin").fadeOut();
            $(".fin").css("background", "#f7f7f7");
        }
    });
    $(".fin").click(function(){
        if ($(".tab-fin").css("display")=="block"){
            return false;
        }
        else {
            $(".tab-fin").fadeIn();
            $(".fin").css("background", "#ffffff");
            $(".tab-qur").fadeOut();
            $(".qur").css("background", "#f7f7f7");
        }
    });
    $(".auction-list-provider td").click(function(){
        position = $("body").scrollTop();
        position +=100;
        $(".pop-up-element").css("top", position+"px");
        var size = $("body").css("height");
        $(".overlay").css("height", size);
        $(".overlay").fadeIn();
        $(".pop-up").fadeIn();
        $(".rate").fadeIn();
    });
    $(".button-alt").click(function(){
        $(".rate .detail-bottom form").fadeOut();
        $(".rate .form-alt").fadeIn();
        return false;
    });
    $(".dt-delet").click(function(){
        $(".rate .detail-bottom form").fadeIn();
        $(".rate .form-alt").fadeOut();
        return false;
    });
    $(".add-detail").click(function(){
        position = $("body").scrollTop();
        position +=100;
        $(".new-auction").css("top", position+"px");
        var size = $("body").css("height");
        $(".overlay").css("height", size);
        $(".overlay").fadeIn();
        $(".pop-up").fadeIn();
        $("#provider-window").fadeIn();
        $(".model-span").html($(".model").val());
    });
    $("#auto-details-provider").click(function(){
        $(".new-auction").fadeOut();
        $(".new-auction-auto-details").css("top", position+"px");
        $("#provider-auto-detail").fadeIn();
        return false;
    });
    $("#wheel-provider").click(function(){
        $(".new-auction").fadeOut();
        $(".new-auction-wheel").css("top", position+"px");
        $("#provider-wheel").fadeIn();
        return false;
    });
    $(".auction-list-buyer td").click(function(){
        position = $("body").scrollTop();
        position +=100;
        $(".pop-up-element").css("top", position+"px");
        var size = $("body").css("height");
        $(".overlay").css("height", size);
        $(".overlay").fadeIn();
        $(".pop-up").fadeIn();
        $(".select-provider").fadeIn();
    });
    var closeFunc = function(selector){

        $(selector).parent().addClass("js-disable");
    }
    $(".close-button").click(function(){
        closeFunc(this);
    });
    var thisChecked;
    $(".manuf-check").click(function(){
        thisChecked = $(this).parent();
        if($(this).hasClass("check-disable")) {
            $(".manuf-list").removeClass("js-disable");
        }
        else {
            $(this).parent().find(".country").addClass("js-disable").html("");
            $(this).parent().find(".country-img").attr("src", "");
        }
    });
    $(".manuf-list .select-list-element").click(function(){
        $(thisChecked).find(".country").html($(this).children("span").html());
        $(thisChecked).find(".country-img").attr("src", $(this).children("img").attr("src"));
        $(thisChecked).find(".country").removeClass("js-disable");
        console.log(thisChecked);
        thisChecked.children(".checked").removeClass("check-disable").addClass("check-enable");
        closeFunc(this);
    });
    $(".checked").click(function(){
        if ($(this).hasClass("check-disable") && !$(this).hasClass("manuf-check")) {
            $(this).removeClass("check-disable").addClass("check-enable");
        }
        else {
            $(this).removeClass("check-enable").addClass("check-disable");
        }
    });
    $(".country").click(function(){
        $(".manuf-list").removeClass("js-disable");
        return false;
    });
    $(".select-container").click(function(){
        var heightSelectList = $(this).children(".select-list").height();
        if ($(".select-container").offset().top-$(window).scrollTop()>heightSelectList) {
            var positionSelectList = -heightSelectList;
            $(this).children(".select-list").removeClass("top").addClass("bottom");
        }
        else {
            $(this).children(".select-list").removeClass("bottom").addClass("top");
        }
    });
    $(window).scroll(function(){
        $(".select-container").each(function(){
            var heightSelectList = $(this).children(".select-list").height();
            if ($(".select-container").offset().top-$(window).scrollTop()>heightSelectList) {
                var positionSelectList = -heightSelectList;
                $(this).children(".select-list").removeClass("top").addClass("bottom");
            }
            else {
                $(this).children(".select-list").removeClass("bottom").addClass("top");
            }
        });
    });
    //какая то важная хуйня
     jQuery.expr[':'].Contains = function(a,i,m){
                return jQuery(a).text().toLowerCase().indexOf(m[3].toLowerCase())>=0;
            };
    //какая то важная хуйня
    $(".filter").keyup(function(){
        var filter = $(this).val();
        $(this).parent().find(".select-list li:Contains("+filter+")").show();
        $(this).parent().find(".select-list li:not(:Contains("+filter+"))").hide();
    });
});
$(document).on("click",".list-element span",function(){
    var parentLI = $(this).parent();
    thisSpanHtml = $(this).html();

    //$.get('/auction/getSections/',{'section_id':$(parentLI).attr('data-section_id'),'type_id':type_id},function(data){
    //  if(data.type=='sections')
    //    $(parentLI).html("<span>"+thisSpanHtml+"</span>"+data.str);
    //  else{
    //    //console.log(data.str);
    //    $("#details").html(data.str)
    //    $(".demo").fadeIn();
    //  }
    //},'json');
    $.getJSON(
        "/api/v0/section/?type_id=" + type_id + "&section_id=" + parentLI.data("section_id"),
        function(data){
            var sections = data['sections'],
                parts = data['parts'];
            if(sections.length > 0) {
                var $list = $("<ul>").addClass("nasting");
                for(var i=0; i<sections.length; i++){
                    var s = sections[i];
                    $list.append("<li data-section_id='" + s['id'] + "' class='list-element active'><span>" + s['text'] + "</span></li>");
                }
                parentLI
                    .html("<span>"+thisSpanHtml+"</span>")
                    .append($list);
            }
            if(parts.length > 0) {
                $("#details").empty();
                for(var i=0; i<parts.length; i++){
                    var p = parts[i];
                    $("#details").append($("<li class='demo'><div class='text-wrap'><span>" + p['brand'] + "</span><p>" + p['text'] + "</p></div><div class='choos-button choos-disable'>Выбрать</div></li>"));
                }
                $(".demo").fadeIn();
            }
        }
    );
    //console.log("123");
    if($(this).parent().children(".nasting").children(".list-element").hasClass("disable")){
        $(this).parent().children(".nasting").children(".list-element").removeClass("disable").addClass("active");
        //console.log("1234");
    }
    else if ($(this).parent().children(".nasting").children(".list-element").hasClass("active")){
        $(this).parent().children(".nasting").children(".list-element").removeClass("active").addClass("disable");
    }
});
$(".list-element a").click(function(){
    $(".demo").fadeIn();
    return false;
});
