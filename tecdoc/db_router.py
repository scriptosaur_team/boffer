from django.conf import settings


class TecDocDBRouter:

    tecdoc_db_name = 'tecdoc'
    tecdoc_app_label = 'tecdoc'

    def db_for_read(self, model, **hints):
        if self.tecdoc_db_name in settings.DATABASES:
            if model._meta.app_label == self.tecdoc_app_label:
                return self.tecdoc_db_name
        return None

    def db_for_write(self, model, **hints):
        if self.tecdoc_db_name in settings.DATABASES:
            if model._meta.app_label == self.tecdoc_app_label:
                return self.tecdoc_db_name
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if self.tecdoc_db_name in settings.DATABASES:
            if self.tecdoc_app_label in [obj1._meta.app_label, obj2._meta.app_label]:
                return True
        return None

    def allow_migrate(self, db, app_label, model=None, **hints):
        if self.tecdoc_db_name in settings.DATABASES:
            if db == self.tecdoc_db_name:
                return model._meta.app_label == self.tecdoc_app_label
            elif model._meta.app_label == self.tecdoc_app_label:
                return False
        return None
