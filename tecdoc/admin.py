from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.apps import apps
from .models import *


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ['mfa_mf_nr', 'mfa_brand', 'mfa_mfc_code']
    list_display_links = ['mfa_mf_nr', 'mfa_brand', 'mfa_mfc_code']
    fieldsets = (
        (None, {
            'fields': ('mfa_brand', 'mfa_mfc_code', 'mfa_mf_nr')
        }),
        (_('production'), {
            'fields': ('mfa_pc_mfc', 'mfa_cv_mfc', 'mfa_axl_mfc', 'mfa_eng_mfc', 'mfa_eng_typ')
        })
    )
    list_filter = ['mfa_pc_mfc', 'mfa_cv_mfc', 'mfa_axl_mfc', 'mfa_eng_mfc', 'mfa_eng_typ']


tecdoc_app = apps.get_app_config('tecdoc')
for model in tecdoc_app.models.values():
    if not admin.site.is_registered(model):
        admin.site.register(model)
