# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desidered behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


DEFAULT_SECTION_ID = 10001
DEFAULT_LANG_ID = 16


class AccessoryLists(models.Model):
    acl_art_id = models.IntegerField(db_column='ACL_ART_ID')  # Field name made lowercase.
    acl_nr = models.SmallIntegerField(db_column='ACL_NR')  # Field name made lowercase.
    acl_sort = models.SmallIntegerField(db_column='ACL_SORT')  # Field name made lowercase.
    acl_link_type = models.SmallIntegerField(db_column='ACL_LINK_TYPE', blank=True, null=True)  # Field name made lowercase.
    acl_mfa_id = models.SmallIntegerField(db_column='ACL_MFA_ID', blank=True, null=True)  # Field name made lowercase.
    acl_mod_id = models.IntegerField(db_column='ACL_MOD_ID', blank=True, null=True)  # Field name made lowercase.
    acl_typ_id = models.IntegerField(db_column='ACL_TYP_ID', blank=True, null=True)  # Field name made lowercase.
    acl_eng_id = models.IntegerField(db_column='ACL_ENG_ID', blank=True, null=True)  # Field name made lowercase.
    acl_accessory_art_id = models.IntegerField(db_column='ACL_ACCESSORY_ART_ID', blank=True, null=True)  # Field name made lowercase.
    acl_quantity = models.SmallIntegerField(db_column='ACL_QUANTITY', blank=True, null=True)  # Field name made lowercase.
    acl_ga_id = models.IntegerField(db_column='ACL_GA_ID', blank=True, null=True)  # Field name made lowercase.
    acl_des_id = models.IntegerField(db_column='ACL_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accessory_lists'
        unique_together = (('acl_art_id', 'acl_nr', 'acl_sort'),)


class AclCriteria(models.Model):
    acc_art_id = models.IntegerField(db_column='ACC_ART_ID')  # Field name made lowercase.
    acc_nr = models.SmallIntegerField(db_column='ACC_NR')  # Field name made lowercase.
    acc_sort = models.SmallIntegerField(db_column='ACC_SORT')  # Field name made lowercase.
    acc_seq_nr = models.SmallIntegerField(db_column='ACC_SEQ_NR')  # Field name made lowercase.
    acc_cri_id = models.SmallIntegerField(db_column='ACC_CRI_ID', blank=True, null=True)  # Field name made lowercase.
    acc_value = models.CharField(db_column='ACC_VALUE', max_length=60, blank=True, null=True)  # Field name made lowercase.
    acc_kv_des_id = models.IntegerField(db_column='ACC_KV_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'acl_criteria'
        unique_together = (('acc_art_id', 'acc_nr', 'acc_sort', 'acc_seq_nr'),)


class AliCoordinates(models.Model):
    aco_gra_id = models.CharField(db_column='ACO_GRA_ID', max_length=11)  # Field name made lowercase.
    aco_gra_lng_id = models.SmallIntegerField(db_column='ACO_GRA_LNG_ID')  # Field name made lowercase.
    aco_ali_art_id = models.IntegerField(db_column='ACO_ALI_ART_ID')  # Field name made lowercase.
    aco_ali_sort = models.SmallIntegerField(db_column='ACO_ALI_SORT')  # Field name made lowercase.
    aco_sort = models.SmallIntegerField(db_column='ACO_SORT')  # Field name made lowercase.
    aco_type = models.SmallIntegerField(db_column='ACO_TYPE', blank=True, null=True)  # Field name made lowercase.
    aco_x1 = models.SmallIntegerField(db_column='ACO_X1', blank=True, null=True)  # Field name made lowercase.
    aco_y1 = models.SmallIntegerField(db_column='ACO_Y1', blank=True, null=True)  # Field name made lowercase.
    aco_x2 = models.SmallIntegerField(db_column='ACO_X2', blank=True, null=True)  # Field name made lowercase.
    aco_y2 = models.SmallIntegerField(db_column='ACO_Y2', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ali_coordinates'
        unique_together = (('aco_gra_id', 'aco_gra_lng_id', 'aco_ali_art_id', 'aco_ali_sort', 'aco_sort'),)


class ArtCountrySpecifics(models.Model):
    acs_art_id = models.IntegerField(db_column='ACS_ART_ID')  # Field name made lowercase.
    acs_pack_unit = models.IntegerField(db_column='ACS_PACK_UNIT', blank=True, null=True)  # Field name made lowercase.
    acs_quantity_per_unit = models.IntegerField(db_column='ACS_QUANTITY_PER_UNIT', blank=True, null=True)  # Field name made lowercase.
    acs_kv_status_des_id = models.IntegerField(db_column='ACS_KV_STATUS_DES_ID', blank=True, null=True)  # Field name made lowercase.
    acs_kv_status = models.CharField(db_column='ACS_KV_STATUS', max_length=9, blank=True, null=True)  # Field name made lowercase.
    acs_status_date = models.DateTimeField(db_column='ACS_STATUS_DATE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'art_country_specifics'


class ArtLookup(models.Model):
    arl_art_id = models.IntegerField(db_column='ARL_ART_ID')  # Field name made lowercase.
    arl_search_number = models.CharField(db_column='ARL_SEARCH_NUMBER', max_length=105)  # Field name made lowercase.
    arl_kind = models.CharField(db_column='ARL_KIND', max_length=1)  # Field name made lowercase.
    arl_bra_id = models.SmallIntegerField(db_column='ARL_BRA_ID')  # Field name made lowercase.
    arl_display_nr = models.CharField(db_column='ARL_DISPLAY_NR', max_length=105)  # Field name made lowercase.
    arl_display = models.SmallIntegerField(db_column='ARL_DISPLAY')  # Field name made lowercase.
    arl_block = models.SmallIntegerField(db_column='ARL_BLOCK')  # Field name made lowercase.
    arl_sort = models.SmallIntegerField(db_column='ARL_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'art_lookup'
        unique_together = (('arl_art_id', 'arl_search_number', 'arl_kind', 'arl_bra_id', 'arl_display_nr', 'arl_display', 'arl_block', 'arl_sort'),)


class ArticleCriteria(models.Model):
    acr_art_id = models.IntegerField(db_column='ACR_ART_ID')  # Field name made lowercase.
    acr_ga_id = models.IntegerField(db_column='ACR_GA_ID')  # Field name made lowercase.
    acr_sort = models.SmallIntegerField(db_column='ACR_SORT')  # Field name made lowercase.
    acr_cri_id = models.SmallIntegerField(db_column='ACR_CRI_ID')  # Field name made lowercase.
    acr_value = models.CharField(db_column='ACR_VALUE', max_length=60, blank=True, null=True)  # Field name made lowercase.
    acr_kv_des_id = models.IntegerField(db_column='ACR_KV_DES_ID', blank=True, null=True)  # Field name made lowercase.
    acr_display = models.SmallIntegerField(db_column='ACR_DISPLAY', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'article_criteria'
        unique_together = (('acr_art_id', 'acr_ga_id', 'acr_sort'),)


class ArticleInfo(models.Model):
    ain_art_id = models.IntegerField(db_column='AIN_ART_ID')  # Field name made lowercase.
    ain_ga_id = models.IntegerField(db_column='AIN_GA_ID')  # Field name made lowercase.
    ain_sort = models.SmallIntegerField(db_column='AIN_SORT')  # Field name made lowercase.
    ain_kv_type = models.CharField(db_column='AIN_KV_TYPE', max_length=9)  # Field name made lowercase.
    ain_display = models.SmallIntegerField(db_column='AIN_DISPLAY')  # Field name made lowercase.
    ain_tmo_id = models.IntegerField(db_column='AIN_TMO_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'article_info'
        unique_together = (('ain_art_id', 'ain_ga_id', 'ain_sort', 'ain_kv_type', 'ain_display', 'ain_tmo_id'),)


class ArticleListCriteria(models.Model):
    alc_ali_art_id = models.IntegerField(db_column='ALC_ALI_ART_ID')  # Field name made lowercase.
    alc_ali_sort = models.SmallIntegerField(db_column='ALC_ALI_SORT')  # Field name made lowercase.
    alc_sort = models.SmallIntegerField(db_column='ALC_SORT')  # Field name made lowercase.
    alc_cri_id = models.SmallIntegerField(db_column='ALC_CRI_ID')  # Field name made lowercase.
    alc_value = models.CharField(db_column='ALC_VALUE', max_length=60)  # Field name made lowercase.
    alc_kv_des_id = models.IntegerField(db_column='ALC_KV_DES_ID')  # Field name made lowercase.
    alc_typ_id = models.IntegerField(db_column='ALC_TYP_ID')  # Field name made lowercase.
    alc_eng_id = models.IntegerField(db_column='ALC_ENG_ID')  # Field name made lowercase.
    alc_axl_id = models.IntegerField(db_column='ALC_AXL_ID')  # Field name made lowercase.
    alc_mrk_id = models.IntegerField(db_column='ALC_MRK_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'article_list_criteria'
        unique_together = (('alc_ali_art_id', 'alc_ali_sort', 'alc_sort', 'alc_cri_id', 'alc_value', 'alc_kv_des_id', 'alc_typ_id', 'alc_eng_id', 'alc_axl_id', 'alc_mrk_id'),)


class ArticleLists(models.Model):
    ali_art_id = models.IntegerField(db_column='ALI_ART_ID')  # Field name made lowercase.
    ali_sort = models.SmallIntegerField(db_column='ALI_SORT')  # Field name made lowercase.
    ali_art_id_component = models.IntegerField(db_column='ALI_ART_ID_COMPONENT', blank=True, null=True)  # Field name made lowercase.
    ali_quantity = models.SmallIntegerField(db_column='ALI_QUANTITY', blank=True, null=True)  # Field name made lowercase.
    ali_ga_id = models.IntegerField(db_column='ALI_GA_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'article_lists'
        unique_together = (('ali_art_id', 'ali_sort'),)


class PartManager(models.Manager):
    def select(self, type_id, section_id):
        section_id = section_id or DEFAULT_SECTION_ID
        sql = """
          SELECT
            ART_ID,
            ART_ARTICLE_NR,
            SUP_BRAND,
            SUP_ID,
            DES_TEXTS.TEX_TEXT AS ART_COMPLETE_DES_TEXT
          FROM ARTICLES
          INNER JOIN DESIGNATIONS ON DESIGNATIONS.DES_ID = ART_COMPLETE_DES_ID AND DESIGNATIONS.DES_LNG_ID = 16
          INNER JOIN DES_TEXTS ON DES_TEXTS.TEX_ID = DESIGNATIONS.DES_TEX_ID
          INNER JOIN SUPPLIERS ON SUP_ID = ART_SUP_ID
          WHERE ART_ID IN (
            SELECT LA_ART_ID
            FROM LINK_GA_STR
            INNER JOIN LINK_LA_TYP ON LAT_TYP_ID = {} AND LAT_GA_ID = LGS_GA_ID
            INNER JOIN LINK_ART ON LA_ID = LAT_LA_ID
            INNER JOIN ARTICLES ON ART_ID = LA_ART_ID
            WHERE LGS_STR_ID = {} ORDER BY LA_ART_ID
          )
        """.format(type_id, section_id)
        return self.model.objects.raw(sql)


class Part(models.Model):
    id = models.IntegerField(db_column='ART_ID', primary_key=True)
    art_article_nr = models.CharField(db_column='ART_ARTICLE_NR', max_length=66)
    art_sup_id = models.SmallIntegerField(db_column='ART_SUP_ID', blank=True, null=True)
    art_des_id = models.IntegerField(db_column='ART_DES_ID', blank=True, null=True)
    art_complete_des_id = models.IntegerField(db_column='ART_COMPLETE_DES_ID', blank=True, null=True)
    art_pack_selfservice = models.SmallIntegerField(db_column='ART_PACK_SELFSERVICE', blank=True, null=True)
    art_material_mark = models.SmallIntegerField(db_column='ART_MATERIAL_MARK', blank=True, null=True)
    art_replacement = models.SmallIntegerField(db_column='ART_REPLACEMENT', blank=True, null=True)
    art_accessory = models.SmallIntegerField(db_column='ART_ACCESSORY', blank=True, null=True)
    art_batch_size1 = models.IntegerField(db_column='ART_BATCH_SIZE1', blank=True, null=True)
    art_batch_size2 = models.IntegerField(db_column='ART_BATCH_SIZE2', blank=True, null=True)

    objects = PartManager()

    @property
    def brand(self):
        assert hasattr(self, 'SUP_BRAND'), \
            _('this property requires custom manager method using raw sql e.g. `select()`')
        return self.SUP_BRAND

    @property
    def text(self):
        assert hasattr(self, 'ART_COMPLETE_DES_TEXT'), \
            _('this property requires custom manager method using raw sql e.g. `select()`')
        return self.ART_COMPLETE_DES_TEXT

    class Meta:
        managed = False
        db_table = 'articles'
        verbose_name = _('part')
        verbose_name_plural = _('parts')


class ArticlesNew(models.Model):
    artn_sup_id = models.SmallIntegerField(db_column='ARTN_SUP_ID')  # Field name made lowercase.
    artn_art_id = models.IntegerField(db_column='ARTN_ART_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'articles_new'
        unique_together = (('artn_sup_id', 'artn_art_id'),)


class AxlBrakeSizes(models.Model):
    abs_axl_id = models.IntegerField(db_column='ABS_AXL_ID')  # Field name made lowercase.
    abs_sort = models.SmallIntegerField(db_column='ABS_SORT')  # Field name made lowercase.
    abs_kv_brake_size_des_id = models.IntegerField(db_column='ABS_KV_BRAKE_SIZE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    abs_description = models.CharField(db_column='ABS_DESCRIPTION', max_length=60, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'axl_brake_sizes'
        unique_together = (('abs_axl_id', 'abs_sort'),)


class Axles(models.Model):
    axl_id = models.IntegerField(db_column='AXL_ID', primary_key=True)  # Field name made lowercase.
    axl_description = models.CharField(db_column='AXL_DESCRIPTION', max_length=90, blank=True, null=True)  # Field name made lowercase.
    axl_search_text = models.CharField(db_column='AXL_SEARCH_TEXT', max_length=90, blank=True, null=True)  # Field name made lowercase.
    axl_mma_cds_id = models.IntegerField(db_column='AXL_MMA_CDS_ID', blank=True, null=True)  # Field name made lowercase.
    axl_mod_id = models.IntegerField(db_column='AXL_MOD_ID', blank=True, null=True)  # Field name made lowercase.
    axl_sort = models.IntegerField(db_column='AXL_SORT', blank=True, null=True)  # Field name made lowercase.
    axl_pcon_start = models.IntegerField(db_column='AXL_PCON_START', blank=True, null=True)  # Field name made lowercase.
    axl_pcon_end = models.IntegerField(db_column='AXL_PCON_END', blank=True, null=True)  # Field name made lowercase.
    axl_kv_type_des_id = models.IntegerField(db_column='AXL_KV_TYPE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    axl_kv_style_des_id = models.IntegerField(db_column='AXL_KV_STYLE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    axl_kv_brake_type_des_id = models.IntegerField(db_column='AXL_KV_BRAKE_TYPE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    axl_kv_body_des_id = models.IntegerField(db_column='AXL_KV_BODY_DES_ID', blank=True, null=True)  # Field name made lowercase.
    axl_load_from = models.IntegerField(db_column='AXL_LOAD_FROM', blank=True, null=True)  # Field name made lowercase.
    axl_load_upto = models.IntegerField(db_column='AXL_LOAD_UPTO', blank=True, null=True)  # Field name made lowercase.
    axl_kv_wheel_mount_des_id = models.IntegerField(db_column='AXL_KV_WHEEL_MOUNT_DES_ID', blank=True, null=True)  # Field name made lowercase.
    axl_track_gauge = models.IntegerField(db_column='AXL_TRACK_GAUGE', blank=True, null=True)  # Field name made lowercase.
    axl_hub_system = models.CharField(db_column='AXL_HUB_SYSTEM', max_length=60, blank=True, null=True)  # Field name made lowercase.
    axl_distance_from = models.IntegerField(db_column='AXL_DISTANCE_FROM', blank=True, null=True)  # Field name made lowercase.
    axl_distance_upto = models.IntegerField(db_column='AXL_DISTANCE_UPTO', blank=True, null=True)  # Field name made lowercase.
    axl_search_brake_sizes = models.CharField(db_column='AXL_SEARCH_BRAKE_SIZES', max_length=256, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'axles'


class Brands(models.Model):
    bra_id = models.SmallIntegerField(db_column='BRA_ID', primary_key=True)  # Field name made lowercase.
    bra_mfc_code = models.CharField(db_column='BRA_MFC_CODE', max_length=60, blank=True, null=True)  # Field name made lowercase.
    bra_brand = models.CharField(db_column='BRA_BRAND', max_length=60, blank=True, null=True)  # Field name made lowercase.
    bra_mf_nr = models.IntegerField(db_column='BRA_MF_NR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'brands'


class ConstPatternLookup(models.Model):
    cpl_id = models.IntegerField(db_column='CPL_ID')  # Field name made lowercase.
    cpl_sort = models.SmallIntegerField(db_column='CPL_SORT')  # Field name made lowercase.
    cpl_original_text = models.CharField(db_column='CPL_ORIGINAL_TEXT', max_length=60)  # Field name made lowercase.
    cpl_search_text = models.CharField(db_column='CPL_SEARCH_TEXT', max_length=60)  # Field name made lowercase.
    cpl_kind = models.SmallIntegerField(db_column='CPL_KIND')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'const_pattern_lookup'


class DesText(models.Model):
    id = models.IntegerField(db_column='TEX_ID', primary_key=True)
    tex_text = models.TextField(_('text'), db_column='TEX_TEXT', blank=True, null=True)

    def __str__(self):
        return self.tex_text

    class Meta:
        managed = False
        db_table = 'des_texts'
        verbose_name = _('text data')
        verbose_name_plural = _('text data')


class Countries(models.Model):
    cou_id = models.SmallIntegerField(db_column='COU_ID', primary_key=True)  # Field name made lowercase.
    cou_cc = models.CharField(db_column='COU_CC', max_length=9, blank=True, null=True)  # Field name made lowercase.
    cou_des_id = models.IntegerField(db_column='COU_DES_ID', blank=True, null=True)  # Field name made lowercase.
    cou_currency_code = models.CharField(db_column='COU_CURRENCY_CODE', max_length=9, blank=True, null=True)  # Field name made lowercase.
    cou_iso2 = models.CharField(db_column='COU_ISO2', max_length=6, blank=True, null=True)  # Field name made lowercase.
    cou_is_group = models.SmallIntegerField(db_column='COU_IS_GROUP')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'countries'


class CountryDesignationManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(cds_lng_id=DEFAULT_LANG_ID)


class CountryDesignation(models.Model):
    id = models.IntegerField(db_column='CDS_ID', primary_key=True)
    cds_lng_id = models.SmallIntegerField(_('language id'), db_column='CDS_LNG_ID', default=DEFAULT_LANG_ID)
    cds_tex = models.ForeignKey(DesText, verbose_name=_('text'), db_column='CDS_TEX_ID')

    # objects = CountryDesignationManager()

    def __str__(self):
        return str(self.cds_tex)

    class Meta:
        managed = False
        db_table = 'country_designations'


class Criteria(models.Model):
    cri_id = models.SmallIntegerField(db_column='CRI_ID', primary_key=True)  # Field name made lowercase.
    cri_des_id = models.IntegerField(db_column='CRI_DES_ID')  # Field name made lowercase.
    cri_short_des_id = models.IntegerField(db_column='CRI_SHORT_DES_ID', blank=True, null=True)  # Field name made lowercase.
    cri_unit_des_id = models.IntegerField(db_column='CRI_UNIT_DES_ID', blank=True, null=True)  # Field name made lowercase.
    cri_type = models.CharField(db_column='CRI_TYPE', max_length=1)  # Field name made lowercase.
    cri_kt_id = models.SmallIntegerField(db_column='CRI_KT_ID', blank=True, null=True)  # Field name made lowercase.
    cri_is_interval = models.SmallIntegerField(db_column='CRI_IS_INTERVAL', blank=True, null=True)  # Field name made lowercase.
    cri_successor = models.SmallIntegerField(db_column='CRI_SUCCESSOR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'criteria'


class CvCabs(models.Model):
    cab_id = models.IntegerField(db_column='CAB_ID', primary_key=True)  # Field name made lowercase.
    cab_mod_id = models.IntegerField(db_column='CAB_MOD_ID', blank=True, null=True)  # Field name made lowercase.
    cab_cds_id = models.IntegerField(db_column='CAB_CDS_ID')  # Field name made lowercase.
    cab_mmc_cds_id = models.IntegerField(db_column='CAB_MMC_CDS_ID', blank=True, null=True)  # Field name made lowercase.
    cab_kv_size_des_id = models.IntegerField(db_column='CAB_KV_SIZE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    cab_pcon_start = models.IntegerField(db_column='CAB_PCON_START', blank=True, null=True)  # Field name made lowercase.
    cab_pcon_end = models.IntegerField(db_column='CAB_PCON_END', blank=True, null=True)  # Field name made lowercase.
    x_cab_design = models.CharField(db_column='X_CAB_DESIGN', max_length=90, blank=True, null=True)  # Field name made lowercase.
    x_cab_length = models.IntegerField(db_column='X_CAB_LENGTH', blank=True, null=True)  # Field name made lowercase.
    x_cab_height = models.IntegerField(db_column='X_CAB_HEIGHT', blank=True, null=True)  # Field name made lowercase.
    x_cab_width = models.IntegerField(db_column='X_CAB_WIDTH', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cv_cabs'


class CvMarks(models.Model):
    mrk_id = models.IntegerField(db_column='MRK_ID', primary_key=True)  # Field name made lowercase.
    mrk_designation = models.CharField(db_column='MRK_DESIGNATION', max_length=60, blank=True, null=True)  # Field name made lowercase.
    mrk_search_text = models.CharField(db_column='MRK_SEARCH_TEXT', max_length=60, blank=True, null=True)  # Field name made lowercase.
    mrk_mfa_id = models.SmallIntegerField(db_column='MRK_MFA_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cv_marks'


class CvSecondaryTypes(models.Model):
    cst_typ_id = models.IntegerField(db_column='CST_TYP_ID')  # Field name made lowercase.
    cst_subnr = models.SmallIntegerField(db_column='CST_SUBNR')  # Field name made lowercase.
    cst_sort = models.SmallIntegerField(db_column='CST_SORT')  # Field name made lowercase.
    cst_cds_id = models.IntegerField(db_column='CST_CDS_ID', blank=True, null=True)  # Field name made lowercase.
    cst_pcon_start = models.IntegerField(db_column='CST_PCON_START', blank=True, null=True)  # Field name made lowercase.
    cst_pcon_end = models.IntegerField(db_column='CST_PCON_END', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cv_secondary_types'
        unique_together = (('cst_typ_id', 'cst_subnr'),)


class Designation(models.Model):
    id = models.IntegerField(db_column='DES_ID', primary_key=True)
    des_lng_id = models.SmallIntegerField(_('language id'), db_column='DES_LNG_ID')
    des_tex = models.ForeignKey(DesText, verbose_name=_('text data'), db_column='DES_TEX_ID')

    def __str__(self):
        return self.des_tex

    class Meta:
        managed = False
        db_table = 'designations'
        unique_together = (('id', 'des_lng_id'),)
        verbose_name = _('language designation')
        verbose_name_plural = _('language designations')


class Language(models.Model):
    id = models.SmallIntegerField(db_column='LNG_ID', primary_key=True)
    lng_des = models.ForeignKey(Designation, verbose_name=_('name'), db_column='LNG_DES_ID', blank=True, null=True)
    lng_iso2 = models.CharField(db_column='LNG_ISO2', max_length=6, blank=True, null=True)
    lng_codepage = models.CharField(_('codepage'), db_column='LNG_CODEPAGE', max_length=30, blank=True, null=True)

    def __str__(self):
        return self.lng_des

    class Meta:
        managed = False
        db_table = 'languages'


class DocTypes(models.Model):
    doc_type = models.SmallIntegerField(db_column='DOC_TYPE', primary_key=True)  # Field name made lowercase.
    doc_extension = models.CharField(db_column='DOC_EXTENSION', max_length=9, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'doc_types'


class EngCountrySpecifics(models.Model):
    enc_eng_id = models.IntegerField(db_column='ENC_ENG_ID')  # Field name made lowercase.
    enc_cou_id = models.SmallIntegerField(db_column='ENC_COU_ID')  # Field name made lowercase.
    enc_pcon_start = models.IntegerField(db_column='ENC_PCON_START', blank=True, null=True)  # Field name made lowercase.
    enc_pcon_end = models.IntegerField(db_column='ENC_PCON_END', blank=True, null=True)  # Field name made lowercase.
    enc_kw_from = models.IntegerField(db_column='ENC_KW_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_kw_upto = models.IntegerField(db_column='ENC_KW_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_hp_from = models.IntegerField(db_column='ENC_HP_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_hp_upto = models.IntegerField(db_column='ENC_HP_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_valves = models.SmallIntegerField(db_column='ENC_VALVES', blank=True, null=True)  # Field name made lowercase.
    enc_cylinders = models.SmallIntegerField(db_column='ENC_CYLINDERS', blank=True, null=True)  # Field name made lowercase.
    enc_ccm_from = models.IntegerField(db_column='ENC_CCM_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_ccm_upto = models.IntegerField(db_column='ENC_CCM_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_kv_design_des_id = models.IntegerField(db_column='ENC_KV_DESIGN_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_fuel_type_des_id = models.IntegerField(db_column='ENC_KV_FUEL_TYPE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_fuel_supply_des_id = models.IntegerField(db_column='ENC_KV_FUEL_SUPPLY_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_description = models.CharField(db_column='ENC_DESCRIPTION', max_length=90, blank=True, null=True)  # Field name made lowercase.
    enc_kv_engine_des_id = models.IntegerField(db_column='ENC_KV_ENGINE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kw_rpm_from = models.IntegerField(db_column='ENC_KW_RPM_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_kw_rpm_upto = models.IntegerField(db_column='ENC_KW_RPM_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_torque_from = models.IntegerField(db_column='ENC_TORQUE_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_torque_upto = models.IntegerField(db_column='ENC_TORQUE_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_torque_rpm_from = models.IntegerField(db_column='ENC_TORQUE_RPM_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_torque_rpm_upto = models.IntegerField(db_column='ENC_TORQUE_RPM_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_compression_from = models.FloatField(db_column='ENC_COMPRESSION_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_compression_upto = models.FloatField(db_column='ENC_COMPRESSION_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_drilling = models.FloatField(db_column='ENC_DRILLING', blank=True, null=True)  # Field name made lowercase.
    enc_extension = models.FloatField(db_column='ENC_EXTENSION', blank=True, null=True)  # Field name made lowercase.
    enc_crankshaft = models.SmallIntegerField(db_column='ENC_CRANKSHAFT', blank=True, null=True)  # Field name made lowercase.
    enc_kv_charge_des_id = models.IntegerField(db_column='ENC_KV_CHARGE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_gas_norm_des_id = models.IntegerField(db_column='ENC_KV_GAS_NORM_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_cylinders_des_id = models.IntegerField(db_column='ENC_KV_CYLINDERS_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_control_des_id = models.IntegerField(db_column='ENC_KV_CONTROL_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_valve_control_des_id = models.IntegerField(db_column='ENC_KV_VALVE_CONTROL_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_kv_cooling_des_id = models.IntegerField(db_column='ENC_KV_COOLING_DES_ID', blank=True, null=True)  # Field name made lowercase.
    enc_ccm_tax_from = models.IntegerField(db_column='ENC_CCM_TAX_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_ccm_tax_upto = models.IntegerField(db_column='ENC_CCM_TAX_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_litres_tax_from = models.FloatField(db_column='ENC_LITRES_TAX_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_litres_tax_upto = models.FloatField(db_column='ENC_LITRES_TAX_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_litres_from = models.FloatField(db_column='ENC_LITRES_FROM', blank=True, null=True)  # Field name made lowercase.
    enc_litres_upto = models.FloatField(db_column='ENC_LITRES_UPTO', blank=True, null=True)  # Field name made lowercase.
    enc_kv_use_des_id = models.IntegerField(db_column='ENC_KV_USE_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eng_country_specifics'
        unique_together = (('enc_eng_id', 'enc_cou_id'),)


class EngLookup(models.Model):
    enl_eng_id = models.IntegerField(db_column='ENL_ENG_ID', blank=True, null=True)  # Field name made lowercase.
    enl_search_text = models.CharField(db_column='ENL_SEARCH_TEXT', max_length=180, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'eng_lookup'


class Engines(models.Model):
    eng_id = models.IntegerField(db_column='ENG_ID', primary_key=True)  # Field name made lowercase.
    eng_mfa_id = models.SmallIntegerField(db_column='ENG_MFA_ID', blank=True, null=True)  # Field name made lowercase.
    eng_code = models.CharField(db_column='ENG_CODE', max_length=180)  # Field name made lowercase.
    eng_pcon_start = models.IntegerField(db_column='ENG_PCON_START', blank=True, null=True)  # Field name made lowercase.
    eng_pcon_end = models.IntegerField(db_column='ENG_PCON_END', blank=True, null=True)  # Field name made lowercase.
    eng_kw_from = models.IntegerField(db_column='ENG_KW_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_kw_upto = models.IntegerField(db_column='ENG_KW_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_hp_from = models.IntegerField(db_column='ENG_HP_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_hp_upto = models.IntegerField(db_column='ENG_HP_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_valves = models.SmallIntegerField(db_column='ENG_VALVES', blank=True, null=True)  # Field name made lowercase.
    eng_cylinders = models.SmallIntegerField(db_column='ENG_CYLINDERS', blank=True, null=True)  # Field name made lowercase.
    eng_ccm_from = models.IntegerField(db_column='ENG_CCM_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_ccm_upto = models.IntegerField(db_column='ENG_CCM_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_kv_design_des_id = models.IntegerField(db_column='ENG_KV_DESIGN_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_fuel_type_des_id = models.IntegerField(db_column='ENG_KV_FUEL_TYPE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_fuel_supply_des_id = models.IntegerField(db_column='ENG_KV_FUEL_SUPPLY_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_description = models.CharField(db_column='ENG_DESCRIPTION', max_length=90, blank=True, null=True)  # Field name made lowercase.
    eng_kv_engine_des_id = models.IntegerField(db_column='ENG_KV_ENGINE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kw_rpm_from = models.IntegerField(db_column='ENG_KW_RPM_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_kw_rpm_upto = models.IntegerField(db_column='ENG_KW_RPM_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_torque_from = models.IntegerField(db_column='ENG_TORQUE_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_torque_upto = models.IntegerField(db_column='ENG_TORQUE_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_torque_rpm_from = models.IntegerField(db_column='ENG_TORQUE_RPM_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_torque_rpm_upto = models.IntegerField(db_column='ENG_TORQUE_RPM_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_compression_from = models.FloatField(db_column='ENG_COMPRESSION_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_compression_upto = models.FloatField(db_column='ENG_COMPRESSION_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_drilling = models.FloatField(db_column='ENG_DRILLING', blank=True, null=True)  # Field name made lowercase.
    eng_extension = models.FloatField(db_column='ENG_EXTENSION', blank=True, null=True)  # Field name made lowercase.
    eng_crankshaft = models.SmallIntegerField(db_column='ENG_CRANKSHAFT', blank=True, null=True)  # Field name made lowercase.
    eng_kv_charge_des_id = models.IntegerField(db_column='ENG_KV_CHARGE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_gas_norm_des_id = models.IntegerField(db_column='ENG_KV_GAS_NORM_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_cylinders_des_id = models.IntegerField(db_column='ENG_KV_CYLINDERS_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_control_des_id = models.IntegerField(db_column='ENG_KV_CONTROL_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_valve_control_des_id = models.IntegerField(db_column='ENG_KV_VALVE_CONTROL_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_kv_cooling_des_id = models.IntegerField(db_column='ENG_KV_COOLING_DES_ID', blank=True, null=True)  # Field name made lowercase.
    eng_ccm_tax_from = models.IntegerField(db_column='ENG_CCM_TAX_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_ccm_tax_upto = models.IntegerField(db_column='ENG_CCM_TAX_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_litres_tax_from = models.FloatField(db_column='ENG_LITRES_TAX_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_litres_tax_upto = models.FloatField(db_column='ENG_LITRES_TAX_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_litres_from = models.FloatField(db_column='ENG_LITRES_FROM', blank=True, null=True)  # Field name made lowercase.
    eng_litres_upto = models.FloatField(db_column='ENG_LITRES_UPTO', blank=True, null=True)  # Field name made lowercase.
    eng_kv_use_des_id = models.IntegerField(db_column='ENG_KV_USE_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'engines'


class ErrTrackKeyValues(models.Model):
    etk_tab_nr = models.SmallIntegerField(db_column='ETK_TAB_NR')  # Field name made lowercase.
    etk_key = models.CharField(db_column='ETK_KEY', max_length=9)  # Field name made lowercase.
    etk_lng_id = models.SmallIntegerField(db_column='ETK_LNG_ID')  # Field name made lowercase.
    etk_sortnr = models.SmallIntegerField(db_column='ETK_SORTNR', blank=True, null=True)  # Field name made lowercase.
    etk_description = models.CharField(db_column='ETK_DESCRIPTION', max_length=180)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'err_track_key_values'
        unique_together = (('etk_tab_nr', 'etk_key', 'etk_lng_id'),)


class Filters(models.Model):
    fil_uss_id = models.IntegerField(db_column='FIL_USS_ID')  # Field name made lowercase.
    fil_kind = models.SmallIntegerField(db_column='FIL_KIND')  # Field name made lowercase.
    fil_value = models.IntegerField(db_column='FIL_VALUE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'filters'
        unique_together = (('fil_uss_id', 'fil_kind', 'fil_value'),)


class GenericArticles(models.Model):
    ga_id = models.IntegerField(db_column='GA_ID', primary_key=True)  # Field name made lowercase.
    ga_nr = models.SmallIntegerField(db_column='GA_NR', blank=True, null=True)  # Field name made lowercase.
    ga_des_id = models.IntegerField(db_column='GA_DES_ID')  # Field name made lowercase.
    ga_des_id_standard = models.IntegerField(db_column='GA_DES_ID_STANDARD')  # Field name made lowercase.
    ga_des_id_assembly = models.IntegerField(db_column='GA_DES_ID_ASSEMBLY', blank=True, null=True)  # Field name made lowercase.
    ga_des_id_intended = models.IntegerField(db_column='GA_DES_ID_INTENDED', blank=True, null=True)  # Field name made lowercase.
    ga_universal = models.SmallIntegerField(db_column='GA_UNIVERSAL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'generic_articles'


class Graphics(models.Model):
    gra_sup_id = models.SmallIntegerField(db_column='GRA_SUP_ID', blank=True, null=True)  # Field name made lowercase.
    gra_id = models.CharField(db_column='GRA_ID', max_length=11)  # Field name made lowercase.
    gra_doc_type = models.IntegerField(db_column='GRA_DOC_TYPE', blank=True, null=True)  # Field name made lowercase.
    gra_lng_id = models.SmallIntegerField(db_column='GRA_LNG_ID')  # Field name made lowercase.
    gra_grd_id = models.IntegerField(db_column='GRA_GRD_ID', blank=True, null=True)  # Field name made lowercase.
    gra_type = models.SmallIntegerField(db_column='GRA_TYPE', blank=True, null=True)  # Field name made lowercase.
    gra_norm = models.CharField(db_column='GRA_NORM', max_length=9, blank=True, null=True)  # Field name made lowercase.
    gra_supplier_nr = models.SmallIntegerField(db_column='GRA_SUPPLIER_NR', blank=True, null=True)  # Field name made lowercase.
    gra_tab_nr = models.SmallIntegerField(db_column='GRA_TAB_NR', blank=True, null=True)  # Field name made lowercase.
    gra_des_id = models.IntegerField(db_column='GRA_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'graphics'
        unique_together = (('gra_id', 'gra_lng_id'),)


class KeyValues(models.Model):
    kv_kt_id = models.SmallIntegerField(db_column='KV_KT_ID')  # Field name made lowercase.
    kv_kv = models.CharField(db_column='KV_KV', max_length=9)  # Field name made lowercase.
    kv_des_id = models.IntegerField(db_column='KV_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'key_values'
        unique_together = (('kv_kv', 'kv_kt_id'),)


class LaCriteria(models.Model):
    lac_la_id = models.IntegerField(db_column='LAC_LA_ID')  # Field name made lowercase.
    lac_sort = models.IntegerField(db_column='LAC_SORT')  # Field name made lowercase.
    lac_cri_id = models.SmallIntegerField(db_column='LAC_CRI_ID')  # Field name made lowercase.
    lac_value = models.CharField(db_column='LAC_VALUE', max_length=60, blank=True, null=True)  # Field name made lowercase.
    lac_kv_des_id = models.IntegerField(db_column='LAC_KV_DES_ID', blank=True, null=True)  # Field name made lowercase.
    lac_display = models.SmallIntegerField(db_column='LAC_DISPLAY', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'la_criteria'
        unique_together = (('lac_la_id', 'lac_sort'),)


class LaInfo(models.Model):
    lin_la_id = models.IntegerField(db_column='LIN_LA_ID')  # Field name made lowercase.
    lin_sort = models.SmallIntegerField(db_column='LIN_SORT')  # Field name made lowercase.
    lin_kv_type = models.CharField(db_column='LIN_KV_TYPE', max_length=9)  # Field name made lowercase.
    lin_display = models.SmallIntegerField(db_column='LIN_DISPLAY')  # Field name made lowercase.
    lin_tmo_id = models.IntegerField(db_column='LIN_TMO_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'la_info'
        unique_together = (('lin_la_id', 'lin_sort', 'lin_kv_type'),)


class LinkArt(models.Model):
    la_id = models.IntegerField(db_column='LA_ID', primary_key=True)  # Field name made lowercase.
    la_art_id = models.IntegerField(db_column='LA_ART_ID')  # Field name made lowercase.
    la_ga_id = models.IntegerField(db_column='LA_GA_ID')  # Field name made lowercase.
    la_sort = models.IntegerField(db_column='LA_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_art'


class LinkArtGa(models.Model):
    lag_art_id = models.IntegerField(db_column='LAG_ART_ID')  # Field name made lowercase.
    lag_ga_id = models.IntegerField(db_column='LAG_GA_ID')  # Field name made lowercase.
    lag_sup_id = models.SmallIntegerField(db_column='LAG_SUP_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_art_ga'
        unique_together = (('lag_art_id', 'lag_ga_id'),)


class LinkCabTyp(models.Model):
    lct_typ_id = models.IntegerField(db_column='LCT_TYP_ID')  # Field name made lowercase.
    lct_nr = models.SmallIntegerField(db_column='LCT_NR')  # Field name made lowercase.
    lct_cab_id = models.IntegerField(db_column='LCT_CAB_ID')  # Field name made lowercase.
    lct_pcon_start = models.IntegerField(db_column='LCT_PCON_START')  # Field name made lowercase.
    lct_pcon_end = models.IntegerField(db_column='LCT_PCON_END')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_cab_typ'
        unique_together = (('lct_typ_id', 'lct_nr', 'lct_cab_id', 'lct_pcon_start', 'lct_pcon_end'),)


class LinkGaCri(models.Model):
    lgc_ga_nr = models.SmallIntegerField(db_column='LGC_GA_NR')  # Field name made lowercase.
    lgc_cri_id = models.SmallIntegerField(db_column='LGC_CRI_ID')  # Field name made lowercase.
    lgc_sort = models.SmallIntegerField(db_column='LGC_SORT')  # Field name made lowercase.
    lgc_suggestion = models.SmallIntegerField(db_column='LGC_SUGGESTION')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_ga_cri'
        unique_together = (('lgc_ga_nr', 'lgc_cri_id', 'lgc_sort'),)


class LinkGaStr(models.Model):
    lgs_str_id = models.IntegerField(db_column='LGS_STR_ID')  # Field name made lowercase.
    lgs_ga_id = models.IntegerField(db_column='LGS_GA_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_ga_str'
        unique_together = (('lgs_str_id', 'lgs_ga_id'),)


class LinkGraArt(models.Model):
    lga_art_id = models.IntegerField(db_column='LGA_ART_ID')  # Field name made lowercase.
    lga_sort = models.SmallIntegerField(db_column='LGA_SORT')  # Field name made lowercase.
    lga_gra_id = models.CharField(db_column='LGA_GRA_ID', max_length=11)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_gra_art'
        unique_together = (('lga_art_id', 'lga_sort'),)


class LinkGraLa(models.Model):
    lgl_la_id = models.IntegerField(db_column='LGL_LA_ID')  # Field name made lowercase.
    lgl_typ_id = models.IntegerField(db_column='LGL_TYP_ID')  # Field name made lowercase.
    lgl_eng_id = models.IntegerField(db_column='LGL_ENG_ID')  # Field name made lowercase.
    lgl_axl_id = models.IntegerField(db_column='LGL_AXL_ID')  # Field name made lowercase.
    lgl_mrk_id = models.IntegerField(db_column='LGL_MRK_ID')  # Field name made lowercase.
    lgl_sort = models.SmallIntegerField(db_column='LGL_SORT')  # Field name made lowercase.
    lgl_gra_id = models.CharField(db_column='LGL_GRA_ID', max_length=11)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_gra_la'
        unique_together = (('lgl_la_id', 'lgl_typ_id', 'lgl_eng_id', 'lgl_axl_id', 'lgl_mrk_id', 'lgl_sort'),)


class LinkLaAxl(models.Model):
    laa_la_id = models.IntegerField(db_column='LAA_LA_ID')  # Field name made lowercase.
    laa_axl_id = models.IntegerField(db_column='LAA_AXL_ID')  # Field name made lowercase.
    laa_ga_id = models.IntegerField(db_column='LAA_GA_ID')  # Field name made lowercase.
    laa_sup_id = models.SmallIntegerField(db_column='LAA_SUP_ID', blank=True, null=True)  # Field name made lowercase.
    laa_sort = models.IntegerField(db_column='LAA_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_axl'
        unique_together = (('laa_ga_id', 'laa_la_id', 'laa_sort', 'laa_axl_id'),)


class LinkLaAxlNew(models.Model):
    laan_la_id = models.IntegerField(db_column='LAAN_LA_ID')  # Field name made lowercase.
    laan_axl_id = models.IntegerField(db_column='LAAN_AXL_ID')  # Field name made lowercase.
    laan_ga_id = models.IntegerField(db_column='LAAN_GA_ID')  # Field name made lowercase.
    laan_sup_id = models.SmallIntegerField(db_column='LAAN_SUP_ID')  # Field name made lowercase.
    laan_sort = models.IntegerField(db_column='LAAN_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_axl_new'
        unique_together = (('laan_la_id', 'laan_axl_id', 'laan_ga_id', 'laan_sup_id', 'laan_sort'),)


class LinkLaEng(models.Model):
    lae_la_id = models.IntegerField(db_column='LAE_LA_ID')  # Field name made lowercase.
    lae_eng_id = models.IntegerField(db_column='LAE_ENG_ID')  # Field name made lowercase.
    lae_ga_id = models.IntegerField(db_column='LAE_GA_ID')  # Field name made lowercase.
    lae_sup_id = models.SmallIntegerField(db_column='LAE_SUP_ID', blank=True, null=True)  # Field name made lowercase.
    lae_sort = models.IntegerField(db_column='LAE_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_eng'
        unique_together = (('lae_eng_id', 'lae_ga_id', 'lae_la_id', 'lae_sort'),)


class LinkLaEngNew(models.Model):
    laen_sup_id = models.SmallIntegerField(db_column='LAEN_SUP_ID')  # Field name made lowercase.
    laen_ga_id = models.IntegerField(db_column='LAEN_GA_ID')  # Field name made lowercase.
    laen_la_id = models.IntegerField(db_column='LAEN_LA_ID')  # Field name made lowercase.
    laen_eng_id = models.IntegerField(db_column='LAEN_ENG_ID')  # Field name made lowercase.
    laen_sort = models.IntegerField(db_column='LAEN_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_eng_new'
        unique_together = (('laen_sup_id', 'laen_ga_id', 'laen_la_id', 'laen_eng_id', 'laen_sort'),)


class LinkLaMrk(models.Model):
    lam_la_id = models.IntegerField(db_column='LAM_LA_ID')  # Field name made lowercase.
    lam_mrk_id = models.IntegerField(db_column='LAM_MRK_ID')  # Field name made lowercase.
    lam_ga_id = models.IntegerField(db_column='LAM_GA_ID')  # Field name made lowercase.
    lam_sup_id = models.SmallIntegerField(db_column='LAM_SUP_ID', blank=True, null=True)  # Field name made lowercase.
    lam_sort = models.IntegerField(db_column='LAM_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_mrk'
        unique_together = (('lam_mrk_id', 'lam_ga_id', 'lam_la_id', 'lam_sort'),)


class LinkLaMrkNew(models.Model):
    lamn_la_id = models.IntegerField(db_column='LAMN_LA_ID')  # Field name made lowercase.
    lamn_mrk_id = models.IntegerField(db_column='LAMN_MRK_ID')  # Field name made lowercase.
    lamn_ga_id = models.IntegerField(db_column='LAMN_GA_ID')  # Field name made lowercase.
    lamn_sup_id = models.SmallIntegerField(db_column='LAMN_SUP_ID')  # Field name made lowercase.
    lamn_sort = models.IntegerField(db_column='LAMN_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_mrk_new'
        unique_together = (('lamn_la_id', 'lamn_mrk_id', 'lamn_ga_id', 'lamn_sup_id', 'lamn_sort'),)


class LinkLaTyp(models.Model):
    lat_typ_id = models.IntegerField(db_column='LAT_TYP_ID')  # Field name made lowercase.
    lat_la_id = models.IntegerField(db_column='LAT_LA_ID')  # Field name made lowercase.
    lat_ga_id = models.IntegerField(db_column='LAT_GA_ID')  # Field name made lowercase.
    lat_sup_id = models.SmallIntegerField(db_column='LAT_SUP_ID', blank=True, null=True)  # Field name made lowercase.
    lat_sort = models.IntegerField(db_column='LAT_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_typ'
        unique_together = (('lat_typ_id', 'lat_ga_id', 'lat_la_id', 'lat_sort'),)


class LinkLaTypNew(models.Model):
    latn_sup_id = models.SmallIntegerField(db_column='LATN_SUP_ID')  # Field name made lowercase.
    latn_ga_id = models.IntegerField(db_column='LATN_GA_ID')  # Field name made lowercase.
    latn_typ_id = models.IntegerField(db_column='LATN_TYP_ID')  # Field name made lowercase.
    latn_la_id = models.IntegerField(db_column='LATN_LA_ID')  # Field name made lowercase.
    latn_sort = models.IntegerField(db_column='LATN_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_la_typ_new'
        unique_together = (('latn_sup_id', 'latn_ga_id', 'latn_typ_id', 'latn_la_id', 'latn_sort'),)


class LinkShoStr(models.Model):
    lss_sho_id = models.SmallIntegerField(db_column='LSS_SHO_ID')  # Field name made lowercase.
    lss_str_id = models.IntegerField(db_column='LSS_STR_ID')  # Field name made lowercase.
    lss_expand = models.SmallIntegerField(db_column='LSS_EXPAND', blank=True, null=True)  # Field name made lowercase.
    lss_level = models.SmallIntegerField(db_column='LSS_LEVEL')  # Field name made lowercase.
    lss_sort = models.SmallIntegerField(db_column='LSS_SORT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_sho_str'
        unique_together = (('lss_sho_id', 'lss_str_id', 'lss_level'),)


class LinkShoStrType(models.Model):
    lst_str_type = models.SmallIntegerField(db_column='LST_STR_TYPE')  # Field name made lowercase.
    lst_sho_id = models.SmallIntegerField(db_column='LST_SHO_ID')  # Field name made lowercase.
    lst_sort = models.SmallIntegerField(db_column='LST_SORT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_sho_str_type'
        unique_together = (('lst_str_type', 'lst_sho_id'),)


class LinkTypEng(models.Model):
    lte_typ_id = models.IntegerField(db_column='LTE_TYP_ID')  # Field name made lowercase.
    lte_nr = models.SmallIntegerField(db_column='LTE_NR')  # Field name made lowercase.
    lte_eng_id = models.IntegerField(db_column='LTE_ENG_ID')  # Field name made lowercase.
    lte_pcon_start = models.IntegerField(db_column='LTE_PCON_START', blank=True, null=True)  # Field name made lowercase.
    lte_pcon_end = models.IntegerField(db_column='LTE_PCON_END', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_typ_eng'
        unique_together = (('lte_typ_id', 'lte_nr', 'lte_eng_id'),)


class LinkTypMrk(models.Model):
    lmk_typ_id = models.IntegerField(db_column='LMK_TYP_ID')  # Field name made lowercase.
    lmk_mrk_id = models.IntegerField(db_column='LMK_MRK_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'link_typ_mrk'
        unique_together = (('lmk_typ_id', 'lmk_mrk_id'),)


class Manufacturer(models.Model):
    id = models.SmallIntegerField(db_column='MFA_ID', primary_key=True)
    mfa_pc_mfc = models.BooleanField(_('personal car'), db_column='MFA_PC_MFC')
    mfa_cv_mfc = models.BooleanField(_('cargo vehicle'), db_column='MFA_CV_MFC')
    mfa_axl_mfc = models.BooleanField(_('axes'), db_column='MFA_AXL_MFC')
    mfa_eng_mfc = models.BooleanField(_('engines'), db_column='MFA_ENG_MFC')
    mfa_eng_typ = models.SmallIntegerField(_('engine type'), db_column='MFA_ENG_TYP', blank=True, null=True)
    mfa_mfc_code = models.SlugField(_('code'), db_column='MFA_MFC_CODE', max_length=30)
    mfa_brand = models.CharField(_('brand'), db_column='MFA_BRAND', max_length=60)
    mfa_mf_nr = models.IntegerField(_('tecdoc number'), db_column='MFA_MF_NR', blank=True, null=True)

    def __str__(self):
        return self.mfa_brand

    class Meta:
        managed = False
        db_table = 'manufacturers'
        ordering = ['mfa_brand']
        verbose_name = _('manufacturer')
        verbose_name_plural = _('manufacturers')


class ModTypLookup(models.Model):
    mtl_typ_id = models.IntegerField(db_column='MTL_TYP_ID')  # Field name made lowercase.
    mtl_lng_id = models.SmallIntegerField(db_column='MTL_LNG_ID')  # Field name made lowercase.
    mtl_search_text = models.CharField(db_column='MTL_SEARCH_TEXT', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'mod_typ_lookup'


class ModelManager(models.Manager):

    query_tpl = """
                  SELECT MOD_ID, TEX_TEXT AS MOD_CDS_TEXT, MOD_PCON_START, MOD_PCON_END FROM MODELS
                  INNER JOIN COUNTRY_DESIGNATIONS ON CDS_ID = MOD_CDS_ID
                  INNER JOIN DES_TEXTS ON TEX_ID = CDS_TEX_ID
                  WHERE CDS_LNG_ID = 16 {}
                  ORDER BY MOD_CDS_TEXT
                """

    def data(self):
        return self.model.objects.raw(self.query_tpl.format(""))

    def by_manufacturer(self, manufacturer_id):
        try:
            m_id = int(manufacturer_id)
        except TypeError:
            return self.data()
        return self.model.objects.raw(
            self.query_tpl.format(
                " AND MOD_MFA_ID = {}".format(m_id)
            )
        )


class Model(models.Model):
    id = models.IntegerField(db_column='MOD_ID', primary_key=True)
    mod_mfa = models.ForeignKey(Manufacturer, db_column='MOD_MFA_ID', blank=True, null=True,
                                verbose_name=_('manufacturer'))
    mod_cds_id = models.IntegerField(_('designation id'), db_column='MOD_CDS_ID', blank=True, null=True)
    mod_pcon_start = models.IntegerField(_('production start'), db_column='MOD_PCON_START', blank=True, null=True)
    mod_pcon_end = models.IntegerField(_('production end'), db_column='MOD_PCON_END', blank=True, null=True)
    mod_pc = models.BooleanField(_('personal car'), db_column='MOD_PC')
    mod_cv = models.BooleanField(_('cargo vehicle'), db_column='MOD_CV')
    mod_axl = models.SmallIntegerField(db_column='MOD_AXL', blank=True, null=True)

    objects = ModelManager()

    def __str__(self):
        return str(self.mod_cds_id)

    @property
    def text(self):
        assert self.MOD_CDS_TEXT, _("this property requires custom manager method using raw sql e.g. `data()`")
        return self.MOD_CDS_TEXT

    @property
    def year_start(self):
        return str(self.mod_pcon_start)[:4]

    class Meta:
        managed = False
        db_table = 'models'
        ordering = ['-mod_pcon_start']
        verbose_name = _('model')
        verbose_name_plural = _('models')


class NumberplatesNl(models.Model):
    nnl_numberplate = models.CharField(db_column='NNL_NUMBERPLATE', max_length=8)  # Field name made lowercase.
    nnl_typ_id = models.IntegerField(db_column='NNL_TYP_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'numberplates_nl'
        unique_together = (('nnl_numberplate', 'nnl_typ_id'),)


class Parameters(models.Model):
    par_name = models.CharField(db_column='PAR_NAME', primary_key=True, max_length=60)  # Field name made lowercase.
    par_value = models.TextField(db_column='PAR_VALUE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'parameters'


class Prices(models.Model):
    pri_art_id = models.IntegerField(db_column='PRI_ART_ID')  # Field name made lowercase.
    pri_kv_price_type = models.CharField(db_column='PRI_KV_PRICE_TYPE', max_length=9)  # Field name made lowercase.
    pri_price = models.CharField(db_column='PRI_PRICE', max_length=30, blank=True, null=True)  # Field name made lowercase.
    pri_kv_price_unit_des_id = models.IntegerField(db_column='PRI_KV_PRICE_UNIT_DES_ID')  # Field name made lowercase.
    pri_kv_quantity_unit_des_id = models.IntegerField(db_column='PRI_KV_QUANTITY_UNIT_DES_ID')  # Field name made lowercase.
    pri_val_start = models.DateTimeField(db_column='PRI_VAL_START', blank=True, null=True)  # Field name made lowercase.
    pri_val_end = models.DateTimeField(db_column='PRI_VAL_END', blank=True, null=True)  # Field name made lowercase.
    pri_currency_code = models.CharField(db_column='PRI_CURRENCY_CODE', max_length=9)  # Field name made lowercase.
    pri_rebate = models.CharField(db_column='PRI_REBATE', max_length=15, blank=True, null=True)  # Field name made lowercase.
    pri_discount_flag = models.SmallIntegerField(db_column='PRI_DISCOUNT_FLAG')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prices'


class SectionManager(models.Manager):

    query_tpl = """
            SELECT
                STR_ID,
                TEX_TEXT AS STR_DES_TEXT,
                STR_ID_PARENT,
                STR_LEVEL,
                STR_SORT,
                STR_NODE_NR,
                IF(EXISTS(
                    SELECT * FROM SEARCH_TREE AS SEARCH_TREE2
                    WHERE SEARCH_TREE2.STR_ID_PARENT <=> SEARCH_TREE.STR_ID LIMIT 1
                ), 1, 0) AS DESCENDANTS
                FROM SEARCH_TREE
            INNER JOIN DESIGNATIONS ON DES_ID = STR_DES_ID
            INNER JOIN DES_TEXTS ON TEX_ID = DES_TEX_ID
            WHERE
                STR_ID != 13771
                AND STR_ID_PARENT <=> {}
                AND DES_LNG_ID = 16
                {}
            """

    def data(self):
        return self.model.objects.raw(self.query_tpl.format(DEFAULT_SECTION_ID, ""))

    def select(self, type_id, section_id=None):
        try:
            t_id = int(type_id)
        except TypeError:
            return self.data()
        try:
            s_id = int(section_id)
        except TypeError:
            s_id = DEFAULT_SECTION_ID
        return self.model.objects.raw(
            self.query_tpl.format(
                s_id,
                """
                AND EXISTS (
                    SELECT * FROM LINK_GA_STR
                    INNER JOIN LINK_LA_TYP ON LAT_TYP_ID = {} AND LAT_GA_ID = LGS_GA_ID
                    INNER JOIN LINK_ART ON LA_ID = LAT_LA_ID
                    WHERE LGS_STR_ID = STR_ID LIMIT 1
                    )
                ORDER BY STR_DES_TEXT
                """.format(t_id)
            )
        )


class Section(models.Model):
    id = models.IntegerField(db_column='STR_ID', primary_key=True)
    str_id_parent = models.IntegerField(db_column='STR_ID_PARENT', blank=True, null=True)
    str_type = models.SmallIntegerField(db_column='STR_TYPE', blank=True, null=True)
    str_level = models.SmallIntegerField(db_column='STR_LEVEL', blank=True, null=True)
    str_des_id = models.IntegerField(db_column='STR_DES_ID', blank=True, null=True)
    str_sort = models.SmallIntegerField(db_column='STR_SORT', blank=True, null=True)
    str_node_nr = models.IntegerField(db_column='STR_NODE_NR', blank=True, null=True)

    objects = SectionManager()

    @property
    def text(self):
        assert hasattr(self, 'STR_DES_TEXT'),\
            _("this property requires custom manager method using raw sql e.g. `by_type()`")
        return self.STR_DES_TEXT

    class Meta:
        managed = False
        db_table = 'search_tree'


class Shortcuts(models.Model):
    sho_id = models.SmallIntegerField(db_column='SHO_ID', primary_key=True)  # Field name made lowercase.
    sho_des_id = models.IntegerField(db_column='SHO_DES_ID')  # Field name made lowercase.
    sho_picture = models.CharField(db_column='SHO_PICTURE', max_length=60)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'shortcuts'


class StrFamilyTree(models.Model):
    sft_ancestor_str_id = models.IntegerField(db_column='SFT_ANCESTOR_STR_ID')  # Field name made lowercase.
    sft_descendant_str_id = models.IntegerField(db_column='SFT_DESCENDANT_STR_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'str_family_tree'
        unique_together = (('sft_ancestor_str_id', 'sft_descendant_str_id'),)


class StrLookup(models.Model):
    stl_lng_id = models.SmallIntegerField(db_column='STL_LNG_ID')  # Field name made lowercase.
    stl_search_text = models.CharField(db_column='STL_SEARCH_TEXT', max_length=180)  # Field name made lowercase.
    stl_str_id = models.IntegerField(db_column='STL_STR_ID')  # Field name made lowercase.
    stl_ga_id = models.IntegerField(db_column='STL_GA_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'str_lookup'


class SupersededArticles(models.Model):
    sua_art_id = models.IntegerField(db_column='SUA_ART_ID')  # Field name made lowercase.
    sua_number = models.CharField(db_column='SUA_NUMBER', max_length=66)  # Field name made lowercase.
    sua_sort = models.IntegerField(db_column='SUA_SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'superseded_articles'
        unique_together = (('sua_art_id', 'sua_number'),)


class SupplierAddresses(models.Model):
    sad_sup_id = models.SmallIntegerField(db_column='SAD_SUP_ID')  # Field name made lowercase.
    sad_type_of_address = models.CharField(db_column='SAD_TYPE_OF_ADDRESS', max_length=9)  # Field name made lowercase.
    sad_cou_id = models.SmallIntegerField(db_column='SAD_COU_ID')  # Field name made lowercase.
    sad_name1 = models.CharField(db_column='SAD_NAME1', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_name2 = models.CharField(db_column='SAD_NAME2', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_street1 = models.CharField(db_column='SAD_STREET1', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_street2 = models.CharField(db_column='SAD_STREET2', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_pob = models.CharField(db_column='SAD_POB', max_length=30, blank=True, null=True)  # Field name made lowercase.
    sad_cou_id_postal = models.SmallIntegerField(db_column='SAD_COU_ID_POSTAL', blank=True, null=True)  # Field name made lowercase.
    sad_postal_code_place = models.CharField(db_column='SAD_POSTAL_CODE_PLACE', max_length=24, blank=True, null=True)  # Field name made lowercase.
    sad_postal_code_pob = models.CharField(db_column='SAD_POSTAL_CODE_POB', max_length=24, blank=True, null=True)  # Field name made lowercase.
    sad_postal_code_cust = models.CharField(db_column='SAD_POSTAL_CODE_CUST', max_length=24, blank=True, null=True)  # Field name made lowercase.
    sad_city1 = models.CharField(db_column='SAD_CITY1', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_city2 = models.CharField(db_column='SAD_CITY2', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_tel = models.CharField(db_column='SAD_TEL', max_length=120, blank=True, null=True)  # Field name made lowercase.
    sad_fax = models.CharField(db_column='SAD_FAX', max_length=60, blank=True, null=True)  # Field name made lowercase.
    sad_email = models.CharField(db_column='SAD_EMAIL', max_length=180, blank=True, null=True)  # Field name made lowercase.
    sad_web = models.CharField(db_column='SAD_WEB', max_length=180, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'supplier_addresses'
        unique_together = (('sad_sup_id', 'sad_type_of_address', 'sad_cou_id'),)


class SupplierLogos(models.Model):
    slo_id = models.SmallIntegerField(db_column='SLO_ID', primary_key=True)  # Field name made lowercase.
    slo_sup_id = models.SmallIntegerField(db_column='SLO_SUP_ID')  # Field name made lowercase.
    slo_lng_id = models.SmallIntegerField(db_column='SLO_LNG_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'supplier_logos'


class Suppliers(models.Model):
    sup_id = models.SmallIntegerField(db_column='SUP_ID', primary_key=True)  # Field name made lowercase.
    sup_brand = models.CharField(db_column='SUP_BRAND', max_length=60, blank=True, null=True)  # Field name made lowercase.
    sup_supplier_nr = models.SmallIntegerField(db_column='SUP_SUPPLIER_NR', blank=True, null=True)  # Field name made lowercase.
    sup_cou_id = models.SmallIntegerField(db_column='SUP_COU_ID', blank=True, null=True)  # Field name made lowercase.
    sup_is_hess = models.SmallIntegerField(db_column='SUP_IS_HESS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'suppliers'


class TextModuleTexts(models.Model):
    tmt_id = models.IntegerField(db_column='TMT_ID', primary_key=True)  # Field name made lowercase.
    tmt_text = models.TextField(db_column='TMT_TEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'text_module_texts'


class TextModules(models.Model):
    tmo_id = models.IntegerField(db_column='TMO_ID')  # Field name made lowercase.
    tmo_lng_id = models.SmallIntegerField(db_column='TMO_LNG_ID')  # Field name made lowercase.
    tmo_fixed = models.SmallIntegerField(db_column='TMO_FIXED')  # Field name made lowercase.
    tmo_tmt_id = models.IntegerField(db_column='TMO_TMT_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'text_modules'
        unique_together = (('tmo_id', 'tmo_lng_id'),)


class TypCountrySpecifics(models.Model):
    tyc_typ_id = models.IntegerField(db_column='TYC_TYP_ID')  # Field name made lowercase.
    tyc_cou_id = models.SmallIntegerField(db_column='TYC_COU_ID')  # Field name made lowercase.
    tyc_pcon_start = models.IntegerField(db_column='TYC_PCON_START', blank=True, null=True)  # Field name made lowercase.
    tyc_pcon_end = models.IntegerField(db_column='TYC_PCON_END', blank=True, null=True)  # Field name made lowercase.
    tyc_kw_from = models.IntegerField(db_column='TYC_KW_FROM', blank=True, null=True)  # Field name made lowercase.
    tyc_kw_upto = models.IntegerField(db_column='TYC_KW_UPTO', blank=True, null=True)  # Field name made lowercase.
    tyc_hp_from = models.IntegerField(db_column='TYC_HP_FROM', blank=True, null=True)  # Field name made lowercase.
    tyc_hp_upto = models.IntegerField(db_column='TYC_HP_UPTO', blank=True, null=True)  # Field name made lowercase.
    tyc_ccm = models.IntegerField(db_column='TYC_CCM', blank=True, null=True)  # Field name made lowercase.
    tyc_cylinders = models.SmallIntegerField(db_column='TYC_CYLINDERS', blank=True, null=True)  # Field name made lowercase.
    tyc_doors = models.SmallIntegerField(db_column='TYC_DOORS', blank=True, null=True)  # Field name made lowercase.
    tyc_tank = models.SmallIntegerField(db_column='TYC_TANK', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_voltage_des_id = models.IntegerField(db_column='TYC_KV_VOLTAGE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_abs_des_id = models.IntegerField(db_column='TYC_KV_ABS_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_asr_des_id = models.IntegerField(db_column='TYC_KV_ASR_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_engine_des_id = models.IntegerField(db_column='TYC_KV_ENGINE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_brake_type_des_id = models.IntegerField(db_column='TYC_KV_BRAKE_TYPE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_brake_syst_des_id = models.IntegerField(db_column='TYC_KV_BRAKE_SYST_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_catalyst_des_id = models.IntegerField(db_column='TYC_KV_CATALYST_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_body_des_id = models.IntegerField(db_column='TYC_KV_BODY_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_steering_des_id = models.IntegerField(db_column='TYC_KV_STEERING_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_steering_side_des_id = models.IntegerField(db_column='TYC_KV_STEERING_SIDE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_max_weight = models.FloatField(db_column='TYC_MAX_WEIGHT', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_model_des_id = models.IntegerField(db_column='TYC_KV_MODEL_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_axle_des_id = models.IntegerField(db_column='TYC_KV_AXLE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_ccm_tax = models.IntegerField(db_column='TYC_CCM_TAX', blank=True, null=True)  # Field name made lowercase.
    tyc_litres = models.FloatField(db_column='TYC_LITRES', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_drive_des_id = models.IntegerField(db_column='TYC_KV_DRIVE_DES_ID', blank=True, null=True)  # Field name made lowercase.
    tyc_kv_trans_des_id = models.IntegerField(db_column='TYC_KV_TRANS_DES_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'typ_country_specifics'
        unique_together = (('tyc_typ_id', 'tyc_cou_id'),)


class TypSuspensions(models.Model):
    tsu_typ_id = models.IntegerField(db_column='TSU_TYP_ID')  # Field name made lowercase.
    tsu_nr = models.SmallIntegerField(db_column='TSU_NR')  # Field name made lowercase.
    tsu_kv_suspension_des_id = models.IntegerField(db_column='TSU_KV_SUSPENSION_DES_ID')  # Field name made lowercase.
    tsu_kv_axle_pos_des_id = models.IntegerField(db_column='TSU_KV_AXLE_POS_DES_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'typ_suspensions'
        unique_together = (('tsu_typ_id', 'tsu_nr', 'tsu_kv_suspension_des_id', 'tsu_kv_axle_pos_des_id'),)


class TypVoltages(models.Model):
    tvo_typ_id = models.IntegerField(db_column='TVO_TYP_ID')  # Field name made lowercase.
    tvo_nr = models.SmallIntegerField(db_column='TVO_NR')  # Field name made lowercase.
    tvo_kv_voltage_des_id = models.IntegerField(db_column='TVO_KV_VOLTAGE_DES_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'typ_voltages'
        unique_together = (('tvo_typ_id', 'tvo_nr', 'tvo_kv_voltage_des_id'),)


class TypWheelBases(models.Model):
    twb_typ_id = models.IntegerField(db_column='TWB_TYP_ID')  # Field name made lowercase.
    twb_nr = models.SmallIntegerField(db_column='TWB_NR')  # Field name made lowercase.
    twb_wheel_base = models.IntegerField(db_column='TWB_WHEEL_BASE')  # Field name made lowercase.
    twb_kv_axle_pos_des_id = models.IntegerField(db_column='TWB_KV_AXLE_POS_DES_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'typ_wheel_bases'
        unique_together = (('twb_typ_id', 'twb_nr', 'twb_wheel_base', 'twb_kv_axle_pos_des_id'),)


class TypeNumbers(models.Model):
    tyn_typ_id = models.IntegerField(db_column='TYN_TYP_ID')  # Field name made lowercase.
    tyn_search_text = models.CharField(db_column='TYN_SEARCH_TEXT', max_length=60)  # Field name made lowercase.
    tyn_kind = models.SmallIntegerField(db_column='TYN_KIND')  # Field name made lowercase.
    tyn_display_nr = models.CharField(db_column='TYN_DISPLAY_NR', max_length=60)  # Field name made lowercase.
    tyn_gop_nr = models.CharField(db_column='TYN_GOP_NR', max_length=75)  # Field name made lowercase.
    tyn_gop_start = models.IntegerField(db_column='TYN_GOP_START')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'type_numbers'
        unique_together = (('tyn_typ_id', 'tyn_search_text', 'tyn_kind', 'tyn_display_nr', 'tyn_gop_nr', 'tyn_gop_start'),)


class TypeManager(models.Manager):

    query_tpl = """
SELECT TYP_ID, DES_TEXTS.TEX_TEXT AS TYP_CDS_TEXT, TYP_PCON_START, TYP_PCON_END, TYP_CCM, TYP_KW_FROM, TYP_KW_UPTO,
    TYP_HP_FROM, TYP_HP_UPTO, TYP_CYLINDERS, ENGINES.ENG_CODE, DES_TEXTS2.TEX_TEXT AS TYP_ENGINE_DES_TEXT,
    DES_TEXTS3.TEX_TEXT AS TYP_FUEL_DES_TEXT, IFNULL(DES_TEXTS4.TEX_TEXT, DES_TEXTS5.TEX_TEXT) AS TYP_BODY_DES_TEXT,
    DES_TEXTS6.TEX_TEXT AS TYP_AXLE_DES_TEXT, TYP_MAX_WEIGHT FROM TYPES
INNER JOIN COUNTRY_DESIGNATIONS ON COUNTRY_DESIGNATIONS.CDS_ID = TYP_CDS_ID AND COUNTRY_DESIGNATIONS.CDS_LNG_ID = 16
INNER JOIN DES_TEXTS ON DES_TEXTS.TEX_ID = COUNTRY_DESIGNATIONS.CDS_TEX_ID
LEFT JOIN DESIGNATIONS ON DESIGNATIONS.DES_ID = TYP_KV_ENGINE_DES_ID AND DESIGNATIONS.DES_LNG_ID = 16
LEFT JOIN DES_TEXTS AS DES_TEXTS2 ON DES_TEXTS2.TEX_ID = DESIGNATIONS.DES_TEX_ID
LEFT JOIN DESIGNATIONS AS DESIGNATIONS2 ON DESIGNATIONS2.DES_ID = TYP_KV_FUEL_DES_ID AND DESIGNATIONS2.DES_LNG_ID = 16
LEFT JOIN DES_TEXTS AS DES_TEXTS3 ON DES_TEXTS3.TEX_ID = DESIGNATIONS2.DES_TEX_ID
LEFT JOIN LINK_TYP_ENG ON LTE_TYP_ID = TYP_ID
LEFT JOIN ENGINES ON ENG_ID = LTE_ENG_ID
LEFT JOIN DESIGNATIONS AS DESIGNATIONS3 ON DESIGNATIONS3.DES_ID = TYP_KV_BODY_DES_ID AND DESIGNATIONS3.DES_LNG_ID = 16
LEFT JOIN DES_TEXTS AS DES_TEXTS4 ON DES_TEXTS4.TEX_ID = DESIGNATIONS3.DES_TEX_ID
LEFT JOIN DESIGNATIONS AS DESIGNATIONS4 ON DESIGNATIONS4.DES_ID = TYP_KV_MODEL_DES_ID AND DESIGNATIONS4.DES_LNG_ID = 16
LEFT JOIN DES_TEXTS AS DES_TEXTS5 ON DES_TEXTS5.TEX_ID = DESIGNATIONS4.DES_TEX_ID
LEFT JOIN DESIGNATIONS AS DESIGNATIONS5 ON DESIGNATIONS5.DES_ID = TYP_KV_AXLE_DES_ID AND DESIGNATIONS5.DES_LNG_ID = 16
LEFT JOIN DES_TEXTS AS DES_TEXTS6 ON DES_TEXTS6.TEX_ID = DESIGNATIONS5.DES_TEX_ID
{}
ORDER BY TYP_SORT, TYP_CDS_TEXT, TYP_PCON_START, TYP_CCM
                """

    def data(self):
        return self.model.objects.raw(self.query_tpl.format(""))

    def by_model(self, model_id):
        try:
            m_id = int(model_id)
            return self.model.objects.raw(
                    self.query_tpl.format(
                            " WHERE TYP_MOD_ID = {}".format(m_id)
                    )
            )
        except TypeError:
            pass
        return self.data()


class Type(models.Model):
    id = models.IntegerField(db_column='TYP_ID', primary_key=True)
    typ_cds_id = models.IntegerField(db_column='TYP_CDS_ID', blank=True, null=True)
    typ_mmt_cds_id = models.IntegerField(db_column='TYP_MMT_CDS_ID', blank=True, null=True)
    typ_mod_id = models.IntegerField(db_column='TYP_MOD_ID')
    typ_sort = models.IntegerField(db_column='TYP_SORT')
    typ_pcon_start = models.IntegerField(db_column='TYP_PCON_START', blank=True, null=True)
    typ_pcon_end = models.IntegerField(db_column='TYP_PCON_END', blank=True, null=True)
    typ_kw_from = models.IntegerField(db_column='TYP_KW_FROM', blank=True, null=True)
    typ_kw_upto = models.IntegerField(db_column='TYP_KW_UPTO', blank=True, null=True)
    typ_hp_from = models.IntegerField(db_column='TYP_HP_FROM', blank=True, null=True)
    typ_hp_upto = models.IntegerField(db_column='TYP_HP_UPTO', blank=True, null=True)
    typ_ccm = models.IntegerField(db_column='TYP_CCM', blank=True, null=True)
    typ_cylinders = models.SmallIntegerField(db_column='TYP_CYLINDERS', blank=True, null=True)
    typ_doors = models.SmallIntegerField(db_column='TYP_DOORS', blank=True, null=True)
    typ_tank = models.SmallIntegerField(db_column='TYP_TANK', blank=True, null=True)
    typ_kv_voltage_des_id = models.IntegerField(db_column='TYP_KV_VOLTAGE_DES_ID', blank=True, null=True)
    typ_kv_abs_des_id = models.IntegerField(db_column='TYP_KV_ABS_DES_ID', blank=True, null=True)
    typ_kv_asr_des_id = models.IntegerField(db_column='TYP_KV_ASR_DES_ID', blank=True, null=True)
    typ_kv_engine_des_id = models.IntegerField(db_column='TYP_KV_ENGINE_DES_ID', blank=True, null=True)
    typ_kv_brake_type_des_id = models.IntegerField(db_column='TYP_KV_BRAKE_TYPE_DES_ID', blank=True, null=True)
    typ_kv_brake_syst_des_id = models.IntegerField(db_column='TYP_KV_BRAKE_SYST_DES_ID', blank=True, null=True)
    typ_kv_fuel_des_id = models.IntegerField(db_column='TYP_KV_FUEL_DES_ID', blank=True, null=True)
    typ_kv_catalyst_des_id = models.IntegerField(db_column='TYP_KV_CATALYST_DES_ID', blank=True, null=True)
    typ_kv_body_des_id = models.IntegerField(db_column='TYP_KV_BODY_DES_ID', blank=True, null=True)
    typ_kv_steering_des_id = models.IntegerField(db_column='TYP_KV_STEERING_DES_ID', blank=True, null=True)
    typ_kv_steering_side_des_id = models.IntegerField(db_column='TYP_KV_STEERING_SIDE_DES_ID', blank=True, null=True)
    typ_max_weight = models.FloatField(db_column='TYP_MAX_WEIGHT', blank=True, null=True)
    typ_kv_model_des_id = models.IntegerField(db_column='TYP_KV_MODEL_DES_ID', blank=True, null=True)
    typ_kv_axle_des_id = models.IntegerField(db_column='TYP_KV_AXLE_DES_ID', blank=True, null=True)
    typ_ccm_tax = models.IntegerField(db_column='TYP_CCM_TAX', blank=True, null=True)
    typ_litres = models.FloatField(db_column='TYP_LITRES', blank=True, null=True)
    typ_kv_drive_des_id = models.IntegerField(db_column='TYP_KV_DRIVE_DES_ID', blank=True, null=True)
    typ_kv_trans_des_id = models.IntegerField(db_column='TYP_KV_TRANS_DES_ID', blank=True, null=True)
    typ_kv_fuel_supply_des_id = models.IntegerField(db_column='TYP_KV_FUEL_SUPPLY_DES_ID', blank=True, null=True)
    typ_valves = models.SmallIntegerField(db_column='TYP_VALVES', blank=True, null=True)
    typ_rt_exists = models.SmallIntegerField(db_column='TYP_RT_EXISTS', blank=True, null=True)

    objects = TypeManager()

    @property
    def cds_text(self):
        assert self.TYP_CDS_TEXT, _("this property requires custom manager method using raw sql e.g. `data()`")
        return self.TYP_CDS_TEXT

    @property
    def engine_code(self):
        return self.ENG_CODE or ''

    @property
    def text(self):
        assert self.TYP_ENGINE_DES_TEXT, _("this property requires custom manager method using raw sql e.g. `data()`")
        return self.TYP_ENGINE_DES_TEXT

    @property
    def description(self):
        return "{} {} {}".format(
            self.cds_text,
            self.engine_code,
            self.text
        )

    class Meta:
        managed = False
        db_table = 'types'
        verbose_name = _('engine type')
        verbose_name_plural = _('engine types')


class UtilityDirect(models.Model):
    utd_art_id = models.IntegerField(db_column='UTD_ART_ID')  # Field name made lowercase.
    utd_text = models.TextField(db_column='UTD_TEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'utility_direct'
