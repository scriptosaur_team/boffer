from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^profile/$', ProfileView.as_view(), name='profile'),
    url(r'^auction/$', AuctionListView.as_view(), name='auction_list'),
    url(r'^items/$', ItemListView.as_view(), name='item_list'),
    url(r'^news/$', NewsView.as_view(), name='news'),
]
