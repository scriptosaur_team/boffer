from django import template
from django.core.urlresolvers import reverse_lazy

register = template.Library()


@register.inclusion_tag('user/profile_menu.html', takes_context=True)
def profile_menu(context):
    menu = [
        {
            'text': 'Аукционы',
            'url': reverse_lazy('user:auction_list')
        },
        {
            'text': 'Продаваемые товары',
            'url': reverse_lazy('user:item_list')
        },
        {
            'text': 'Личные данные',
            'url': reverse_lazy('user:profile')
        },
        {
            'text': 'Новости',
            'url': reverse_lazy('user:news')
        },
    ]

    return {
        'menu': menu,
        'current_url': context['request'].path
    }
