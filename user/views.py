from django.views.generic import UpdateView, TemplateView, ListView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from auction.models import Auction
from .models import BofferUser
from .forms import ProfileForm


class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'user/profile.html'
    form_class = ProfileForm
    success_url = reverse_lazy('user:profile')

    def get_object(self, queryset=None):
        try:
            return BofferUser.objects.get(id=self.request.user.id)
        except (BofferUser.DoesNotExist, AttributeError):
            return None


class AuctionListView(LoginRequiredMixin, ListView):
    template_name = 'user/auction_list.html'
    model = Auction

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class ItemListView(LoginRequiredMixin, TemplateView):
    template_name = 'user/item_list.html'


class NewsView(LoginRequiredMixin, TemplateView):
    template_name = 'user/news.html'
