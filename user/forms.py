from django import forms
from .models import BofferUser


class ProfileForm(forms.ModelForm):
    class Meta:
        model = BofferUser
        fields = [
            'first_name',
            'last_name',
            'email',
            'phone',
            'town',
            'account',
        ]
